all: test build

build: build-auth build-doc build-stats

build-auth:
	go build -o auth.out pkg/auth/main.go

build-doc:
	go build -o doc.out pkg/doctor/main.go

build-stats:
	go build -o stats.out pkg/stats/main.go

run-doc: build-doc
	./doc.out

run-auth: build-auth
	./auth.out

run-stats: build-stats
	./stats.out

test:
	go test ./...

install:
	go get -d -v ./...
	go install -v ./...

clean:
	go clean
	rm ${OUTPUT_FILE}
