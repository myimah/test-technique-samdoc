# Test technique samdoc

J'ai essayé de faire un `Dockerfile`, mais en vain. Les applications se lancent, mais ne se connectent pas à la base de donnée.
Donc, pour lancer les serveurs il faut utiliser les commandes suivantes :
- serveur d'authentification: `make run-auth`
- serveur d'api des médecins: `make run-doc`
- serveur de statistiques: `make run-stats`

# Endpoints:
## Postman:
Vous trouverez dans le dossier [postman](/postman), deux fichiers l'un contient la collection de requêtes qui pointent toute sur un endpoint de l'application ainsi qu'un fichier qui contient les variables d'environnement pour pouvoir exécuter ces requêtes. Il est donc nécessaire d'importer ces deux fichiers.

## Insomnia
Vous trouverez dans le dossier [insomnia](/insomnia), un seul fichier qui contient le `dashboard` d'insomnia qui contient lui aussi les requêtes vers les endpoints de l'application ainsi que les variables d'environnement.

# Authentification:
Par défaut, il n'y a qu'un seul utilisateur (docteur) qui peut se connecter. Avec les identifiants suivant :
- Nom d'utilisateur : `test`
- Mot de passe : `test`

## Disclaimer: 
L'application n'a été testée que sous Linux (Ubuntu 20.01). 