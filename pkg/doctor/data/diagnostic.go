package data

import (
	"database/sql"
	"fmt"
)

//DbDiagnostic a struct that stores the diagnostic
type DbDiagnostic struct {
	DisId int64 `json:"disease_id" validate:"required"`
	VisId int64 `json:"visit_id"`
}

//RetrieveAllDiagnosticsFromVisit
//returns all the diagnostic linked to a visit
func RetrieveAllDiagnosticsFromVisit(visId int64) (interface{}, error) {
	var q, err = db.Prepare("SELECT D.* FROM SD_DIAGNOSE JOIN SD_DISEASE D USING(DIS_NUM) WHERE APT_NUM = ?")

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve diagnostic: %w", err)
	}

	var rows *sql.Rows
	rows, err = q.Query(visId)

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve diagnostic: %w", err)
	}

	var values = make([]interface{}, 0)

	for rows.Next() {
		var row interface{}
		row, err = parseDiseaseSummary(rows)

		if err != nil {
			return nil, fmt.Errorf("failed to retrieve diagnostics: %w", err)
		}

		values = append(values, row)
	}

	return values, nil
}

//DeleteDiagnostic
//Deletes a diagnostic from the database
func DeleteDiagnostic(visId, disId int64) error {
	var q, err = db.Prepare("DELETE FROM SD_DIAGNOSE WHERE DIS_NUM = ? AND APT_NUM = ?")

	if err != nil {
		return fmt.Errorf("failed to delete diagnostic: %w", err)
	}

	var res sql.Result
	res, err = q.Exec(disId, visId)

	if err != nil {
		return fmt.Errorf("failed to delete diagnostic: %w", err)
	}

	if n, _ := res.RowsAffected(); n == 0 {
		return ErrNoRowAffected
	}

	return nil
}

// CheckDiagnosticOwnership
// returns whether the visit is parent of the diagnotic
func CheckDiagnosticOwnership(disId, visitId int64) (bool, error) {
	var q, err = db.Prepare("SELECT COUNT(*) AS c FROM SD_DIAGNOSE WHERE APT_NUM = ? AND DIS_NUM = ?")

	if err != nil {
		return false, fmt.Errorf("failed to retrieve patient: %w", err)
	}
	var row = q.QueryRow(visitId, disId)
	var c = 0

	_ = row.Scan(&c)
	return c != 0, nil
}

//InsertDiagnostic
//Adds a diagnostic inside the database
func InsertDiagnostic(diagnostic DbDiagnostic) error {
	var q, err = db.Prepare("INSERT INTO SD_DIAGNOSE (DIS_NUM, APT_NUM) VALUES (?, ?)")

	if err != nil {
		return fmt.Errorf("failed to add diagnostic: %w", err)
	}

	_, err = q.Exec(diagnostic.DisId, diagnostic.VisId)

	if err != nil {
		return fmt.Errorf("failed to add disease: %w", err)
	}

	return nil
}
