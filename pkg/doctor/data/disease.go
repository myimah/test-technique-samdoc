package data

import (
	"database/sql"
	"fmt"
)

//DbDiseaseSummary a struct that stores a summary of information about a disease
type DbDiseaseSummary struct {
	Num   int64  `json:"num,omitempty"`
	Label string `json:"label,omitempty" validate:"required"`
}

//DbDisease a struct that stores information about a disease
//Does not have any fields yet, but implemented for a future purpose
type DbDisease struct {
	DbDiseaseSummary
}

var CheckDiseaseExists = CheckExists("SD_DISEASE", "DIS_NUM", "disease")

//RetrieveAllDiseases
//returns all the diseases
var RetrieveAllDiseases = RetrieveAll("SD_DISEASE", "disease", []string{"DIS_NUM", "DIS_LABEL"},
	parseDiseaseSummary)

//InsertDisease
//Adds a disease inside the database
func InsertDisease(disease DbDisease) error {
	var q, err = db.Prepare("INSERT INTO SD_DISEASE (DIS_NUM, DIS_LABEL) VALUES (?, ?)")

	if err != nil {
		return fmt.Errorf("failed to add disease: %w", err)
	}

	_, err = q.Exec(disease.Num, disease.Label)

	if err != nil {
		return fmt.Errorf("failed to add disease: %w", err)
	}

	return nil
}

//UpdateDisease
//Updates some information of a disease inside the database
func UpdateDisease(disease DbDisease) error {
	var q, err = db.Prepare("UPDATE SD_DISEASE SET DIS_LABEL = ? WHERE DIS_NUM = ?")

	if err != nil {
		return fmt.Errorf("failed to update disease: %w", err)
	}

	_, err = q.Exec(disease.Label, disease.Num)

	if err != nil {
		return fmt.Errorf("failed to update disease: %w", err)
	}

	return nil
}

//RetrieveDiseaseFromId
//Retrieves a disease from a given id.
var RetrieveDiseaseFromId = Retrieve("SD_DISEASE", "DIS_NUM", "disease", parseDisease)

//DeleteDisease
//Deletes a disease from the database
var DeleteDisease = Delete("SD_DISEASE", "DIS_NUM", "disease")

func parseDisease(row *sql.Row) (interface{}, error) {
	var user = &DbDisease{}

	var err = row.Scan(&user.Num, &user.Label)

	if err == sql.ErrNoRows {
		return nil, ErrNoDiseaseFound
	}

	if err != nil {
		return nil, err
	}

	return user, nil
}

func parseDiseaseSummary(row *sql.Rows) (interface{}, error) {
	var user = &DbDiseaseSummary{}

	var err = row.Scan(&user.Num, &user.Label)

	if err != nil {
		return nil, err
	}

	return user, nil
}
