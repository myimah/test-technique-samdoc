package data

import (
	"database/sql"
	"fmt"
)

//DbMedicineSummary a struct that stores a summary of information about a medicine
type DbMedicineSummary struct {
	Num   int64  `json:"num,omitempty"`
	Label string `json:"label,omitempty" validate:"required"`
}

//DbMedicine a struct that stores information about a medicine
type DbMedicine struct {
	DbMedicineSummary
	Laboratory string `json:"laboratory" validate:"required"`
	Comment    string `json:"comment" validate:"required"`
}

//RetrieveAllMedicines
//returns all the medicines
var RetrieveAllMedicines = RetrieveAll("SD_MEDICINE", "medicine", []string{"MED_NUM", "MED_LABEL"},
	parseMedicineSummary)

var CheckMedicineExists = CheckExists("SD_MEDICINE", "MED_NUM", "medicine")

//InsertMedicine
//Adds a medicine inside the database
func InsertMedicine(medicine DbMedicine) error {
	var q, err = db.Prepare("INSERT INTO SD_MEDICINE (MED_NUM, MED_LABEL, MED_LABORATORY, MED_COMMENT) VALUES (?, ?, ?, ?)")

	if err != nil {
		return fmt.Errorf("failed to add medicine: %w", err)
	}

	_, err = q.Exec(medicine.Num, medicine.Label, medicine.Laboratory, medicine.Comment)

	if err != nil {
		return fmt.Errorf("failed to add medicine: %w", err)
	}

	return nil
}

//UpdateMedicine
//Updates some information of a medicine inside the database
func UpdateMedicine(medicine DbMedicine) error {
	var q, err = db.Prepare("UPDATE SD_MEDICINE SET MED_LABEL = ?, MED_LABORATORY = ?, MED_COMMENT = ? WHERE MED_NUM = ?")

	if err != nil {
		return fmt.Errorf("failed to update medicine: %w", err)
	}

	_, err = q.Exec(medicine.Label, medicine.Laboratory, medicine.Comment, medicine.Num)

	if err != nil {
		return fmt.Errorf("failed to update medicine: %w", err)
	}

	return nil
}

//RetrieveMedicineFromId
//Retrieves a medicine from a given id.
var RetrieveMedicineFromId = Retrieve("SD_MEDICINE", "MED_NUM", "medicine", parseMedicine)

//DeleteMedicine
//Deletes a medicine from the database
var DeleteMedicine = Delete("SD_MEDICINE", "MED_NUM", "medicine")

func parseMedicine(row *sql.Row) (interface{}, error) {
	var user = &DbMedicine{}

	var err = row.Scan(&user.Num, &user.Label, &user.Laboratory, &user.Comment)

	if err == sql.ErrNoRows {
		return nil, ErrNoMedicineFound
	}

	if err != nil {
		return nil, err
	}

	return user, nil
}

func parseMedicineSummary(row *sql.Rows) (interface{}, error) {
	var user = &DbMedicineSummary{}

	var err = row.Scan(&user.Num, &user.Label)

	if err != nil {
		return nil, err
	}

	return user, nil
}
