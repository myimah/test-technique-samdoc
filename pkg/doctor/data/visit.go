package data

import (
	"database/sql"
	"fmt"
)

//DbVisitSummary a struct that stores a summary of information about a visit
type DbVisitSummary struct {
	Num   int64  `json:"num,omitempty"`
	PatId int64  `json:"patId,omitempty"`
	Date  string `json:"date,omitempty" validate:"required"`
}

//DbVisit a struct that stores information about a visit
type DbVisit struct {
	DbVisitSummary
	Reason string `json:"reason" validate:"required"`
}

var CheckVisitExists = CheckExists("SD_APPOINTMENT", "APT_NUM", "visit")

//RetrieveAllVisitFromPatient
//returns all the visit of a patient.
func RetrieveAllVisitFromPatient(patId int64) ([]DbVisitSummary, error) {
	var q, err = db.Prepare("SELECT APT_NUM, PAT_ID, APT_DATE FROM SD_APPOINTMENT WHERE PAT_ID = ?")

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve visits: %w", err)
	}
	var rows *sql.Rows

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve visits: %w", err)
	}

	rows, err = q.Query(patId)
	var visits = make([]DbVisitSummary, 0)

	for rows.Next() {
		var visit DbVisitSummary
		err = rows.Scan(&visit.Num, &visit.PatId, &visit.Date)

		if err != nil {
			return nil, fmt.Errorf("failed to retrieve visits: %w", err)
		}

		visits = append(visits, visit)
	}

	return visits, nil
}

//InsertVisit
//Adds a visit inside the database
func InsertVisit(visit DbVisit) error {
	var q, err = db.Prepare("INSERT INTO SD_APPOINTMENT (APT_NUM, PAT_ID, APT_DATE, APT_REASON) VALUES (?, ?, ?, ?)")

	if err != nil {
		return fmt.Errorf("failed to add visit: %w", err)
	}

	_, err = q.Exec(visit.Num, visit.PatId, visit.Date, visit.Reason)

	if err != nil {
		return fmt.Errorf("failed to add visit: %w", err)
	}

	return nil
}

//UpdateVisit
//Update a visit inside the database
func UpdateVisit(visit DbVisit) error {
	var q, err = db.Prepare("UPDATE SD_APPOINTMENT SET APT_DATE = IF(? = '', APT_DATE, ?), APT_REASON = ? " +
		"WHERE APT_NUM = ?")

	if err != nil {
		return fmt.Errorf("failed to update medicine: %w", err)
	}

	_, err = q.Exec(visit.Date, visit.Date, visit.Reason, visit.Num)

	if err != nil {
		return fmt.Errorf("failed to update medicine: %w", err)
	}

	return nil
}

//RetrieveVisitFromId
//returns a visit from its id
var RetrieveVisitFromId = Retrieve("SD_APPOINTMENT", "APT_NUM", "visit", parseVisit)

//DeleteVisit
//returns a visit from the database
var DeleteVisit = Delete("SD_APPOINTMENT", "APT_NUM", "visit")

//CheckVisitOwnership
//returns whether the visit is linked to the patient
func CheckVisitOwnership(patNum, visNum int64) (bool, error) {
	var q, err = db.Prepare("SELECT COUNT(*) AS pat_count FROM SD_APPOINTMENT WHERE PAT_ID = ? AND APT_NUM = ?")

	if err != nil {
		return false, fmt.Errorf("failed to retrieve patient: %w", err)
	}
	var row = q.QueryRow(patNum, visNum)
	var c = 0

	_ = row.Scan(&c)
	return c != 0, nil
}

func parseVisit(row *sql.Row) (interface{}, error) {
	var visit = &DbVisit{}

	var err = row.Scan(&visit.Num, &visit.PatId, &visit.Date, &visit.Reason)

	if err == sql.ErrNoRows {
		return nil, ErrNoVisitFound
	}

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve visit: %w", err)
	}

	return visit, nil
}
