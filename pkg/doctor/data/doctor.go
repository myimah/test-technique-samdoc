package data

import "fmt"

// CheckPatientOwnership
// returns whether the doctor has the right to access to the patient data
func CheckPatientOwnership(docId, patId int64) (bool, error) {
	var q, err = db.Prepare("SELECT COUNT(*) AS pat_count FROM SD_PATIENT WHERE doc_id = ? AND pat_id = ?")

	if err != nil {
		return false, fmt.Errorf("failed to retrieve patient: %w", err)
	}
	var row = q.QueryRow(docId, patId)
	var userCount = 0

	_ = row.Scan(&userCount)
	return userCount != 0, nil
}

func InsertDoctor(id int64, firstname, lastname string) error {
	var q, err = db.Prepare("INSERT INTO SD_DOCTOR (DOC_ID, DOC_LASTNAME, DOC_FIRSTNAME) VALUES (?, ?, ?)")

	if err != nil {
		return fmt.Errorf("failed to insert doctor: %w", err)
	}

	_, err = q.Exec(id, lastname, firstname)

	if err != nil {
		return fmt.Errorf("failed to insert doctor: %w", err)
	}

	return nil
}
