package data

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"test-technique-samdoc/pkg/utils/config"
	"test-technique-samdoc/pkg/utils/database"
)

var db *sql.DB

// ErrNoPatientFound
// happens when the id doesn't point to a patient
var ErrNoPatientFound = errors.New("no patient found")

//ErrNoMedicineFound
// happens when the id doesn't point to a medicine
var ErrNoMedicineFound = errors.New("no medicine found")

//ErrNoRowAffected
// happens when no rows where changed when update or deleting table in database
var ErrNoRowAffected = errors.New("no row affected")

//ErrNoDiseaseFound
// happens when the id doesn't point to a disease
var ErrNoDiseaseFound = errors.New("no disease found")

//ErrNoVisitFound
// happens when the id doesn't point to a visit
var ErrNoVisitFound = errors.New("no visit found")

//ErrNoPrescriptionFound
// happens when the id doesn't point to a prescription
var ErrNoPrescriptionFound = errors.New("no prescription found")

//Init configuration
//Initialize the database for the main server
func Init(configuration config.DbConfig) error {
	var err error
	db, err = database.CreateConnection(configuration)

	if err != nil {
		return fmt.Errorf("failed to intialize the connection to the database: %w", err)
	}

	return err
}

func InitWithDB(dbConn *sql.DB) {
	db = dbConn
}

//GetDB
//returns the current used database
func GetDB() *sql.DB {
	return db
}

//CheckExists
//returns a function that checks whether the given id is inside the database
func CheckExists(tabName, keyName, valType string) func(int64) (bool, error) {
	return func(id int64) (bool, error) {
		var sqlReq = fmt.Sprintf("SELECT COUNT(*) AS c FROM %s WHERE %s = ?", tabName, keyName)
		var q, err = db.Prepare(sqlReq)

		if err != nil {
			if err == sql.ErrNoRows {
				return false, nil
			}

			return false, fmt.Errorf("failed to retrieve %s: %w", valType, err)
		}

		var row = q.QueryRow(id)
		var count = 0

		err = row.Scan(&count)

		if err != nil {
			if err == sql.ErrNoRows {
				return false, nil
			}

			return false, err
		}

		return count != 0, nil
	}
}

//Retrieve
//returns a function that retrieves a single row from the database from a given id
func Retrieve(tabName, keyName, valType string, parseFun func(*sql.Row) (interface{}, error)) func(int64) (interface{}, error) {
	return func(id int64) (interface{}, error) {
		var req = fmt.Sprintf("SELECT * FROM %s WHERE %s = ?", tabName, keyName)
		var q, err = db.Prepare(req)

		if err != nil {
			return nil, fmt.Errorf("failed to retrieve %s: %w", valType, err)
		}

		var row = q.QueryRow(id)
		return parseFun(row)
	}
}

//RetrieveAll
//returns a function that retrieves all the rows from the database
func RetrieveAll(tabName, valType string, cols []string, parseRow func(*sql.Rows) (interface{}, error)) func() (interface{}, error) {
	return func() (interface{}, error) {
		var rows, err = db.Query(fmt.Sprintf("SELECT %s FROM %s", strings.Join(cols, ","), tabName))

		if err != nil {
			return nil, fmt.Errorf("failed to retrieve %s: %w", valType, err)
		}

		var values = make([]interface{}, 0)

		for rows.Next() {
			row, err := parseRow(rows)

			if err != nil {
				return nil, fmt.Errorf("failed to retrieve %s: %w", valType, err)
			}

			values = append(values, row)
		}

		return values, nil
	}
}

//Delete
//returns a function that deletes a single line from the database from the given id
func Delete(tabName, keyName, valType string) func(int64) error {
	return func(id int64) error {
		var req = fmt.Sprintf("DELETE FROM %s WHERE %s = ?", tabName, keyName)

		var q, err = db.Prepare(req)

		if err != nil {
			return fmt.Errorf("failed to %s patient: %w", valType, err)
		}

		var res sql.Result
		res, err = q.Exec(id)

		if err != nil {
			return fmt.Errorf("failed to %s patient: %w", valType, err)
		}

		if n, _ := res.RowsAffected(); n == 0 {
			return ErrNoRowAffected
		}

		return nil
	}
}
