package data

import (
	"database/sql"
	"fmt"
)

//DbPrescriptionSummary a struct that stores a summary of information about a prescription
type DbPrescriptionSummary struct {
	PreNum   int64 `json:"prescription_num,omitempty"`
	AptNum   int64 `json:"visit_id,omitempty"`
	MedNum   int64 `json:"medicine_num" validate:"required"`
	Duration int   `json:"duration" validate:"required"`
	DailyQty int   `json:"daily_quantity" validate:"required"`
}

//DbPrescription a struct that stores information about a prescription
type DbPrescription struct {
	DbPrescriptionSummary
	Medicine DbMedicine `json:"medicine"`
}

//CheckPrescriptionExists
//returns whether the prescription exists inside the database
var CheckPrescriptionExists = CheckExists("SD_PRESCRIPTION", "PRE_NUM", "prescription")

//DeletePrescription
//Deletes the prescription from the database
var DeletePrescription = Delete("SD_PRESCRIPTION", "PRE_NUM", "prescription")

//RetrievePrescription
//returns a prescription from its id
var RetrievePrescription = Retrieve("SD_V_PRESCRIPTION", "PRE_NUM", "prescription", parsePrescription)

//RetrieveAllPrescriptionFromVisit
//Returns all the prescription made during a single visit
func RetrieveAllPrescriptionFromVisit(visId int64) ([]DbPrescriptionSummary, error) {
	var q, err = db.Prepare("SELECT * FROM SD_PRESCRIPTION WHERE APT_NUM = ?")

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve prescriptions: %w", err)
	}

	var rows *sql.Rows
	rows, err = q.Query(visId)

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve prescriptions: %w", err)
	}

	return parsePrescriptions(rows)
}

//RetrieveAllPrescriptionFromPatient
//Returns all the prescription made for a single patient
func RetrieveAllPrescriptionFromPatient(patId int64) ([]DbPrescriptionSummary, error) {
	var q, err = db.Prepare("SELECT P.* FROM SD_PRESCRIPTION P JOIN SD_APPOINTMENT USING(APT_NUM) " +
		"WHERE PAT_ID = ?")

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve prescriptions: %w", err)
	}
	var rows *sql.Rows
	rows, err = q.Query(patId)

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve prescriptions: %w", err)
	}

	return parsePrescriptions(rows)
}

//CheckPrescriptionOwnership
//returns whether the visit is prescription to the patient
func CheckPrescriptionOwnership(preNum, patId int64) (bool, error) {
	var q, err = db.Prepare("SELECT COUNT(*) AS pat_count FROM SD_PRESCRIPTION JOIN SD_APPOINTMENT " +
		"USING(APT_NUM) WHERE PRE_NUM = ? AND PAT_ID = ?")

	if err != nil {
		return false, fmt.Errorf("failed to retrieve patient: %w", err)
	}
	var row = q.QueryRow(preNum, patId)
	var c = 0

	_ = row.Scan(&c)
	return c != 0, nil
}

//InsertPrescription
//Adds a prescription to the database.
func InsertPrescription(presc DbPrescriptionSummary) error {
	var q, err = db.Prepare("INSERT INTO SD_PRESCRIPTION (PRE_NUM, APT_NUM, MED_NUM, PRE_DURATION, PRE_DAILY_QTY) " +
		"VALUES (?, ?, ?, ?, ?)")

	if err != nil {
		return fmt.Errorf("failed to add prescription: %w", err)
	}

	_, err = q.Exec(presc.PreNum, presc.AptNum, presc.MedNum, presc.Duration, presc.DailyQty)

	if err != nil {
		return fmt.Errorf("failed to add prescription: %w", err)
	}

	return nil
}

//UpdatePrescription
//Updates some information of a prescription inside the database
func UpdatePrescription(presc DbPrescriptionSummary) error {
	var q, err = db.Prepare("UPDATE SD_PRESCRIPTION SET MED_NUM = ?, PRE_DURATION = ?, PRE_DAILY_QTY = ? " +
		"WHERE PRE_NUM = ?")

	if err != nil {
		return fmt.Errorf("failed to update prescription: %w", err)
	}
	fmt.Println(presc)

	_, err = q.Exec(presc.MedNum, presc.Duration, presc.DailyQty, presc.PreNum)

	if err != nil {
		return fmt.Errorf("failed to update prescription: %w", err)
	}

	return nil
}

func parsePrescription(row *sql.Row) (interface{}, error) {
	var presc = &DbPrescription{}

	var err = row.Scan(&presc.MedNum, &presc.PreNum, &presc.AptNum, &presc.DailyQty, &presc.Duration,
		&presc.Medicine.Label, &presc.Medicine.Laboratory, &presc.Medicine.Comment)

	if err == sql.ErrNoRows {
		return nil, ErrNoPrescriptionFound
	}

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve prescription: %w", err)
	}

	return presc, nil
}

func parsePrescriptions(rows *sql.Rows) ([]DbPrescriptionSummary, error) {

	var prescriptions = make([]DbPrescriptionSummary, 0)

	for rows.Next() {
		var presc = DbPrescriptionSummary{}
		var err = rows.Scan(&presc.PreNum, &presc.MedNum, &presc.AptNum, &presc.Duration, &presc.DailyQty)

		if err != nil {
			return nil, fmt.Errorf("failed to retrieve prescriptions: %w", err)
		}

		prescriptions = append(prescriptions, presc)
	}

	return prescriptions, nil
}
