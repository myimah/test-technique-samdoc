package data

import (
	"database/sql"
	"fmt"
)

//DbPatientSummary a struct that stores a summary of information about a patient
type DbPatientSummary struct {
	Id        int64  `json:"id,omitempty"`
	Lastname  string `json:"lastname,omitempty" validate:"required,min=3,max=32"`
	Firstname string `json:"firstname,omitempty" validate:"required,min=3,max=32"`
}

//DbAddress a struct that store the address of a patient
type DbAddress struct {
	Address string `json:"address,omitempty" validate:"required,max=256"`
	City    string `json:"city,omitempty" validate:"required,max=64"`
	Region  string `json:"region,omitempty" validate:"required,max=64"`
	Zipcode string `json:"zipcode,omitempty" validate:"required"`
	Country string `json:"country,omitempty" validate:"required,max=64"`
}

//DbPatient a struct that stores all the information about a patient
type DbPatient struct {
	DbPatientSummary
	DocId     int64  `json:"doc_id,omitempty"`
	Birthdate string `json:"birthdate,omitempty" validate:"required"`
	Gender    string `json:"gender,omitempty" validate:"required"`
	DbAddress
}

var CheckPatientExists = CheckExists("SD_PATIENT", "PAT_ID", "patient")

//RetrievePatientsFromDoctorId
//returns all the patients that a doctor takes care of.
func RetrievePatientsFromDoctorId(id int64) ([]DbPatientSummary, error) {
	var q, err = db.Prepare("SELECT pat_id, pat_firstname, pat_lastname FROM SD_PATIENT WHERE doc_id = ?")

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve patients: %w", err)
	}
	var rows *sql.Rows

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve patients: %w", err)
	}

	rows, err = q.Query(id)
	var patients = make([]DbPatientSummary, 0)

	for rows.Next() {
		var patient DbPatientSummary
		err = rows.Scan(&patient.Id, &patient.Firstname, &patient.Lastname)

		if err != nil {
			return nil, fmt.Errorf("failed to retrieve patients: %w", err)
		}

		patients = append(patients, patient)
	}

	return patients, nil
}

//RetrievePatientFromId
//returns a patient from its id
func RetrievePatientFromId(id int64) (interface{}, error) {
	return Retrieve("SD_PATIENT", "PAT_ID", "patient", parsePatient)(id)
}

//InsertPatient
//Adds a patient inside the database
func InsertPatient(patient DbPatient) error {
	var rq = "INSERT INTO SD_PATIENT(PAT_ID, DOC_ID, PAT_LASTNAME, PAT_FIRSTNAME, PAT_BIRTHDATE, PAT_GENDER, " +
		"PAT_ADDRESS_LINES, PAT_LOCALITY, PAT_REGION, PAT_ZIPCODE, PAT_COUNTRY) " +
		"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"

	var q, err = db.Prepare(rq)
	if err != nil {
		return fmt.Errorf("failed to insert patient: %w", err)
	}

	_, err = q.Exec(patient.Id, patient.DocId, patient.Lastname, patient.Firstname, patient.Birthdate, patient.Gender,
		patient.Address, patient.City, patient.Region, patient.Zipcode, patient.Country)

	if err != nil {
		return fmt.Errorf("failed to insert patient: %w", err)
	}
	return nil
}

//UpdatePatient
//Updates some information of a patient inside the database
func UpdatePatient(patient DbPatient) error {
	var rq = "UPDATE SD_PATIENT SET PAT_ADDRESS_LINES = ?, PAT_LOCALITY = ?, PAT_REGION = ?, " +
		"PAT_ZIPCODE = ?, PAT_COUNTRY = ? WHERE PAT_ID = ?"

	var q, err = db.Prepare(rq)
	if err != nil {
		return fmt.Errorf("failed to update patient: %w", err)
	}

	_, err = q.Exec(patient.Address, patient.City, patient.Region, patient.Zipcode, patient.Country, patient.Id)

	if err != nil {
		return fmt.Errorf("failed to update patient: %w", err)
	}
	return nil
}

//DeletePatient
//Deletes a patient from the database
func DeletePatient(id int64) error {
	return Delete("SD_PATIENT", "PAT_ID", "patient")(id)
}

func parsePatient(row *sql.Row) (interface{}, error) {
	var user = &DbPatient{}

	var err = row.Scan(&user.Id, &user.DocId, &user.Lastname, &user.Firstname, &user.Birthdate, &user.Gender,
		&user.Address, &user.City, &user.Region, &user.Zipcode, &user.Country)

	if err == sql.ErrNoRows {
		return nil, ErrNoPatientFound
	}

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve user: %w", err)
	}

	return user, nil
}
