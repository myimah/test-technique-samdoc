package visit

import (
	"fmt"
	"github.com/bwmarrin/snowflake"
	"github.com/gin-gonic/gin"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/doctor/docapi"
	"test-technique-samdoc/pkg/utils"
	"test-technique-samdoc/pkg/utils/response"
)

var visNode, _ = snowflake.NewNode(436)

//GetVisit
// Endpoint that send a json formatted visit
//
// Path: /api/patient/:num/visit/:visit
//
// Params:
//   - num:   the id of the patient
// 	 - visit: the id of the visit
// Method: GET
var GetVisit = docapi.Get("visit", data.RetrieveVisitFromId)

//DeleteVisit
//Endpoint that deletes a visit from the database
//
// Path: /api/patient/:num/visit/:visit
//
// Params:
//   - num:   the id of the patient
// 	 - visit: the id of the visit
// Method: DELETE
var DeleteVisit = docapi.Delete("visit", data.DeleteVisit)

//GetVisits
// Endpoint that send a json formatted list of visit of a patient.
//
// Path: /api/patient/:num/visit
//
// Params:
//   - num: the id of the patient
// Method: GET
func GetVisits(ctx *gin.Context) {
	var patId = ctx.GetInt64("num")
	docapi.GetAll(func() (interface{}, error) {
		return data.RetrieveAllVisitFromPatient(patId)
	})(ctx)
}

//PostVisit
// Endpoint that receives a json formatted visit and stores it inside the database.
//
// Path: /api/patient/:num/visit
//
// Params:
//   - num: the id of the patient
// Method: POST
func PostVisit(ctx *gin.Context) {
	var patId = ctx.GetInt64("num")
	var visit = ctx.MustGet("visit").(*data.DbVisit)

	visit.Num = visNode.Generate().Int64()

	if !utils.DatetimeRegex.MatchString(visit.Date) {
		response.SendError(ctx, 400,
			fmt.Sprintf("invalid date format: %s, expected: YYYY-MM-DD hh:mm:ss", visit.Date))
		return
	}

	visit.PatId = patId

	var err = data.InsertVisit(*visit)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	var summary = visit.DbVisitSummary
	ctx.JSON(200, summary)
}

//PutVisit
// Endpoint that receives a json formatted visit and updates it inside the database.
//
// Path: /api/patient/:num/visit/:visit
//
// Params:
//   - num:   the id of the patient
// 	 - visit: the id of the visit
// Method: PUT
func PutVisit(ctx *gin.Context) {
	var visit = &data.DbVisit{}
	var num = ctx.GetInt64("visit")
	var err = ctx.BindJSON(visit)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
		return
	}

	visit.Num = num

	if visit.Date != "" && !utils.DatetimeRegex.MatchString(visit.Date) {
		response.SendError(ctx, 400,
			fmt.Sprintf("invalid date format: %s, expected: YYYY-MM-DD hh:mm:ss", visit.Date))
		return
	}

	err = data.UpdateVisit(*visit)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
		return
	}

	var newPatient, _ = data.RetrieveVisitFromId(num)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
		return
	}

	ctx.JSON(200, newPatient)
}
