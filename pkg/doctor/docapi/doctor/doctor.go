package doctor

import (
	"fmt"
	"github.com/gin-gonic/gin"
	authdata "test-technique-samdoc/pkg/auth/data"
	docdata "test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/utils/response"
)

type RegisterBody struct {
	Username  string `json:"username" validate:"required"`
	Password  string `json:"password" validate:"required"`
	Firstname string `json:"firstname" validate:"required"`
	Lastname  string `json:"lastname" validate:"required"`
}

//PostDoctor
// Endpoint that receives a json formatted doctor and stores it inside the database.
//
// Path: /doctor/register/
//
// Method: POST
func PostDoctor(ctx *gin.Context) {
	var account = ctx.MustGet("account").(*RegisterBody)

	fmt.Println(account)
	id, err := authdata.CreateNewUser(account.Username, account.Password)

	if err != nil {
		if err == authdata.ErrUsernameUsed {
			response.SendError(ctx, 409, "username already taken")
		} else {
			response.SendError(ctx, 500, fmt.Sprintf("internal error: %s", err))
		}
		return
	}

	err = docdata.InsertDoctor(id, account.Firstname, account.Lastname)

	if err != nil {
		response.SendError(ctx, 500, fmt.Sprintf("internal error: %s", err))
		return
	}

	ctx.JSON(200, true)
}
