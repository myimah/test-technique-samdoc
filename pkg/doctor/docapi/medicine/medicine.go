package medicine

import (
	"github.com/bwmarrin/snowflake"
	"github.com/gin-gonic/gin"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/doctor/docapi"
	"test-technique-samdoc/pkg/utils/response"
)

var medNode, _ = snowflake.NewNode(875)

//GetMedicines
// Endpoint that send a json formatted list of medicines.
//
// Path: /api/medicine/
//
// Method: GET
var GetMedicines = docapi.GetAll(data.RetrieveAllMedicines)

//GetMedicine
// Endpoint that send a json formatted medicine
//
// Path: /api/medicine/:med
//
// Params:
//   - med: the id of the medicine
// Method: GET
var GetMedicine = docapi.Get("med", data.RetrieveMedicineFromId)

//DeleteMedicine
//Endpoint that deletes a medicine from the database
//
// Path: /api/medicine/:med
//
// Params:
//   - med: the id of the medicine
// Method: DELETE
var DeleteMedicine = docapi.Delete("med", data.DeleteMedicine)

//PostMedicine
// Endpoint that receives a json formatted medicine and stores it inside the database.
//
// Path: /api/medicine/
//
// Method: POST
func PostMedicine(ctx *gin.Context) {
	var medicine = ctx.MustGet("medicine").(*data.DbMedicine)

	medicine.Num = medNode.Generate().Int64()
	var err = data.InsertMedicine(*medicine)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	var summary = medicine.DbMedicineSummary
	ctx.JSON(200, summary)
}

//PutMedicine
//Endpoint that receives a json formatted body and updates the medicine according to it
//
//Path: /api/medicine/:med
//
// Params:
//   - med: the id of the medicine
// Method: PUT
func PutMedicine(ctx *gin.Context) {
	var medicine = &data.DbMedicine{}
	var num = ctx.GetInt64("med")
	var err = ctx.BindJSON(medicine)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	medicine.Num = num
	err = data.UpdateMedicine(*medicine)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	var newPatient, _ = data.RetrieveMedicineFromId(num)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
		return
	}

	ctx.JSON(200, newPatient)
}
