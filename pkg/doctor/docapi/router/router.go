package router

import (
	"github.com/gin-gonic/gin"
	"test-technique-samdoc/pkg/doctor/docapi/diagnostic"
	"test-technique-samdoc/pkg/doctor/docapi/disease"
	"test-technique-samdoc/pkg/doctor/docapi/doctor"
	"test-technique-samdoc/pkg/doctor/docapi/medicine"
	"test-technique-samdoc/pkg/doctor/docapi/patient"
	"test-technique-samdoc/pkg/doctor/docapi/prescription"
	"test-technique-samdoc/pkg/doctor/docapi/visit"
	"test-technique-samdoc/pkg/utils/middleware"
	"test-technique-samdoc/pkg/utils/middleware/validator"
)

//Route engine
// Adds the engine the different routes used for the main server
func Route(engine *gin.Engine) {
	engine.Use(middleware.CheckContentType)

	var doctorGroup = engine.Group("/doctor")
	{
		doctorGroup.POST("/register", validator.RegisterDoctorValidator, doctor.PostDoctor) // adds a new doctor
	}

	var apiGroup = engine.Group("/api")
	apiGroup.Use(middleware.CheckAuth)

	var patientsGroup = apiGroup.Group("/patient")
	{
		patientsGroup.GET("/", patient.GetPatients)                                    // sends a list of patients
		patientsGroup.POST("/", validator.InsertPatientValidator, patient.PostPatient) // adds a new patient to the list

		var patientGroup = patientsGroup.Group("/:num")
		{
			patientGroup.Use(middleware.CheckPatientExists)
			patientGroup.Use(middleware.CheckPatientOwnership)

			patientGroup.GET("/", patient.GetPatient)       // sends the patient
			patientGroup.PUT("/", patient.PutPatient)       // edits the patient
			patientGroup.DELETE("/", patient.DeletePatient) // removes the patient

			var visitsGroup = patientGroup.Group("/visit/")
			{
				visitsGroup.GET("/", visit.GetVisits)                                  // sends the list of visits of a patient
				visitsGroup.POST("/", validator.InsertVisitValidator, visit.PostVisit) // adds a new visit

				var visitGroup = visitsGroup.Group("/:visit")
				{
					visitGroup.Use(middleware.CheckVisitExists)
					visitGroup.Use(middleware.CheckVisitOwnership)

					visitGroup.GET("/", visit.GetVisit)       // sends detail of a visit
					visitGroup.PUT("/", visit.PutVisit)       // edits detail of a visit
					visitGroup.DELETE("/", visit.DeleteVisit) // deletes a visit

					var visPrescriptionsGroup = visitGroup.Group("/prescription")
					{
						visPrescriptionsGroup.GET("/", prescription.GetVisitPrescriptions) // sends the list of the prescriptions given during the visit
						visPrescriptionsGroup.POST("/", validator.InsertPrescriptionValidator,
							prescription.PostPrescription) // adds a new prescription
					}

					var diagnosticsGroup = visitGroup.Group("/diagnostic")
					{
						diagnosticsGroup.GET("/", diagnostic.GetDiagnostics)                                       // sends the diseases diagnosed during the visit
						diagnosticsGroup.POST("/", validator.InsertDiagnosticValidator, diagnostic.PostDiagnostic) // adds the diseases in the diagnosis

						var diagnosticGroup = diagnosticsGroup.Group("/:dis")
						{
							diagnosticGroup.Use(middleware.CheckDiseaseExists)
							diagnosticGroup.Use(middleware.CheckDiagnosticOwnership)

							diagnosticGroup.DELETE("/", diagnostic.DeleteDiagnostic) // removes the disease of the diagnosis
						}
					}
				}
			}

			patientGroup.GET("/prescription/", prescription.GetPatientPrescriptions)

			var prescriptionGroup = patientGroup.Group("/prescription/:presc")
			{
				prescriptionGroup.Use(middleware.CheckPrescriptionExists)
				prescriptionGroup.Use(middleware.CheckPrescriptionOwnership)

				prescriptionGroup.GET("/", prescription.GetPrescription)       // sends the prescription with id `presc`
				prescriptionGroup.PUT("/", prescription.PutPrescription)       // edits the prescription with id `presc`
				prescriptionGroup.DELETE("/", prescription.DeletePrescription) // removes the prescription with id `presc`
			}
		}
	}

	var medicinesGroup = apiGroup.Group("/medicine/")
	{
		medicinesGroup.GET("/", medicine.GetMedicines)                                     // sends the list of registered medicines
		medicinesGroup.POST("/", validator.InsertMedicineValidator, medicine.PostMedicine) // adds a new medicine

		var medicineGroup = medicinesGroup.Group("/:med")
		{
			medicineGroup.Use(middleware.CheckMedicineExists)

			medicineGroup.GET("/", medicine.GetMedicine)       // sends the medicine with id `med`
			medicineGroup.PUT("/", medicine.PutMedicine)       // edits the medicine with id `med`
			medicineGroup.DELETE("/", medicine.DeleteMedicine) // removes the medicine with id `med`

		}
	}

	var diseasesGroup = apiGroup.Group("/disease/")
	{
		diseasesGroup.GET("/", disease.GetDiseases)                                    // sends the list of registered diseases
		diseasesGroup.POST("/", validator.InsertDiseaseValidator, disease.PostDisease) // adds a new disease

		var diseaseGroup = diseasesGroup.Group("/:dis")
		{
			diseaseGroup.Use(middleware.CheckDiseaseExists)

			diseaseGroup.GET("/", disease.GetDisease)       // sends the disease with id `dis`
			diseaseGroup.PUT("/", disease.PutDisease)       // edits the disease with id `dis`
			diseaseGroup.DELETE("/", disease.DeleteDisease) // removes the disease with id `dis`
		}
	}
}
