package diagnostic

import (
	"github.com/gin-gonic/gin"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/doctor/docapi"
	"test-technique-samdoc/pkg/utils/response"
)

//GetDiagnostics
// Endpoint that send a json formatted list of disease of a visit.
//
// Path: /api/patient/:num/visit/:visit/diagnostic/
//
// Params:
//   - num:   the id of the patient
//   - visit  the id of the visit
// Method: GET
func GetDiagnostics(ctx *gin.Context) {
	var visId = ctx.GetInt64("visit")
	docapi.GetAll(func() (interface{}, error) {
		return data.RetrieveAllDiagnosticsFromVisit(visId)
	})(ctx)
}

//DeleteDiagnostic
//Endpoint that deletes a diagnostic from the database
//
// Path: /api/patient/:num/visit/:visit/diagnostic/:dis
//
// Params:
//   - num:   the id of the patient
//   - visit: the id of the prescription
//   - dis:   the id of the diagnosed disease
// Method: DELETE
func DeleteDiagnostic(ctx *gin.Context) {
	var visId = ctx.GetInt64("visit")
	docapi.Delete("dis", func(disId int64) error {
		return data.DeleteDiagnostic(visId, disId)
	})(ctx)
}

//PostDiagnostic
//Endpoint that receives a json formatted body and updates the prescription according to it.
//
// Path: /api/patient/:num/visit/:visit/diagnostic/
//
// Params:
//   - num:   the id of the patient
//   - visit: the id of the prescription
// Method: POST
func PostDiagnostic(ctx *gin.Context) {
	var diagnostic = ctx.MustGet("diagnostic").(*data.DbDiagnostic)

	diagnostic.VisId = ctx.GetInt64("visit")

	var err = data.InsertDiagnostic(*diagnostic)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
		return
	}

	ctx.JSON(200, diagnostic)
}
