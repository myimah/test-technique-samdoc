package diagnostic_test

import (
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/go-playground/assert/v2"
	tc "test-technique-samdoc/pkg/__test_commons"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/doctor/docapi/diagnostic"
	"testing"
)

var sampleData = "123456789,dis2\n8743512313,dis3\n78943513123,dis4\n12354587,dis5\n135486784,dis6\n123123434,dis7\n"

func TestGetDiagnostics__valid(t *testing.T) {
	var ctx, rec = tc.CreateValidRequest("GET", "/patient/1234/visit/1234/diagnostic/")
	ctx.Set("num", int64(1234))
	ctx.Set("visit", int64(1234))
	rec.Code = 0

	tc.DbMock.ExpectPrepare(`SELECT D\.\* FROM SD_DIAGNOSE JOIN SD_DISEASE D USING\(DIS_NUM\) WHERE APT_NUM = \?`).
		ExpectQuery().
		WithArgs(1234).
		WillReturnRows(sqlmock.NewRows([]string{"MED_NUM", "MED_LABEL"}).FromCSVString(sampleData))

	diagnostic.GetDiagnostics(ctx)

	assert.Equal(t, rec.Code, 200)

	var diagnostics []data.DbDiseaseSummary
	tc.ParseBody(t, rec.Body, &diagnostics)

	assert.Equal(t, len(diagnostics), 6)
	assert.Equal(t, diagnostics[0].Label, "dis2")
}

func TestGetDiagnostics__invalid(t *testing.T) {
	var ctx, rec = tc.CreateValidRequest("GET", "/patient/1234/visit/1234/diagnostic/")
	ctx.Set("num", int64(1234))
	ctx.Set("visit", int64(1234))
	rec.Code = 0

	tc.DbMock.ExpectPrepare(`SELECT D\.\* FROM SD_DIAGNOSE JOIN SD_DISEASE D USING\(DIS_NUM\) WHERE APT_NUM = \?`).
		ExpectQuery().
		WithArgs(1234).
		WillReturnError(errors.New("fake internal error"))

	diagnostic.GetDiagnostics(ctx)

	assert.Equal(t, rec.Code, 500)
}

func TestPostDiagnostic(t *testing.T) {
	var diagnosticBody = data.DbDiagnostic{
		DisId: 1234,
	}
	var ctx, rec = tc.CreateValidRequestWithBody("POST", "/patient/1234/visit/1234/diagnosticBody/", diagnosticBody)
	ctx.Set("num", int64(1234))
	ctx.Set("visit", int64(1234))
	ctx.Set("diagnostic", &diagnosticBody) // simulate exist middleware
	rec.Code = 0

	tc.DbMock.ExpectPrepare(`INSERT INTO SD_DIAGNOSE \(DIS_NUM, APT_NUM\) VALUES \(\?, \?\)`).
		ExpectExec().
		WithArgs(int64(1234), int64(1234)).
		WillReturnResult(sqlmock.NewResult(0, 1))

	diagnostic.PostDiagnostic(ctx)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}

	assert.Equal(t, rec.Code, 200)
}

func TestDeleteDiagnostic(t *testing.T) {
	var ctx, rec = tc.CreateValidRequest("DELETE", "/patient/1234/visit/1234/diagnostic/1234")
	ctx.Set("num", int64(1234))
	ctx.Set("visit", int64(1234))
	ctx.Set("dis", int64(1234))
	rec.Code = 0

	tc.DbMock.ExpectPrepare(`DELETE FROM SD_DIAGNOSE WHERE DIS_NUM = \? AND APT_NUM = \?`).
		ExpectExec().
		WithArgs(1234, 1234).
		WillReturnResult(sqlmock.NewResult(0, 1))

	diagnostic.DeleteDiagnostic(ctx)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}

	assert.Equal(t, rec.Code, 200)
}
