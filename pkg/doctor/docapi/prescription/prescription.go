package prescription

import (
	"fmt"
	"github.com/bwmarrin/snowflake"
	"github.com/gin-gonic/gin"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/doctor/docapi"
	"test-technique-samdoc/pkg/utils/response"
)

var preNode, _ = snowflake.NewNode(812)

//GetPrescription
// Endpoint that send a json formatted prescription
//
// Path: /api/patient/:num/prescription/:presc
//
// Params:
//   - num:   the id of the patient
//   - presc: the id of the prescription
// Method: GET
var GetPrescription = docapi.Get("presc", data.RetrievePrescription)

//DeletePrescription
//Endpoint that deletes a prescription from the database
//
// Path: /api/patient/:num/prescription/:presc
//
// Params:
//   - num:   the id of the patient
//   - presc: the id of the prescription
// Method: DELETE
var DeletePrescription = docapi.Delete("presc", data.DeletePrescription)

//GetPatientPrescriptions
// Endpoint that send a json formatted list of prescription of a patient.
//
// Path: /api/patient/:num/prescription/
//
// Params:
//   - num:   the id of the patient
// Method: GET
func GetPatientPrescriptions(ctx *gin.Context) {
	var patNum = ctx.GetInt64("num")
	docapi.GetAll(func() (interface{}, error) {
		return data.RetrieveAllPrescriptionFromPatient(patNum)
	})(ctx)
}

//GetVisitPrescriptions
// Endpoint that send a json formatted list of prescription of a visit.
//
// Path: /api/patient/:num/prescription/:visit
//
// Params:
//   - num:   the id of the patient
//   - visit: the id of the visit
// Method: GET
func GetVisitPrescriptions(ctx *gin.Context) {
	var visitId = ctx.GetInt64("visit")
	docapi.GetAll(func() (interface{}, error) {
		return data.RetrieveAllPrescriptionFromVisit(visitId)
	})(ctx)
}

//PutPrescription
// Endpoint that receives a json formatted body and updates the prescription according to it
//
// Path: /api/patient/:num/prescription/:presc
//
// Params:
//   - num:   the id of the patient
//   - presc: the id of the prescription
// Method: PUT
func PutPrescription(ctx *gin.Context) {
	var presc = &data.DbPrescriptionSummary{}
	var prescId = ctx.GetInt64("presc")
	var err = ctx.BindJSON(presc)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
		return
	}

	presc.PreNum = prescId
	err = data.UpdatePrescription(*presc)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
		return
	}

	var newPatient, _ = data.RetrievePrescription(prescId)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
		return
	}

	ctx.JSON(200, newPatient.(*data.DbPrescription).DbPrescriptionSummary)
}

//PostPrescription
// Endpoint that receives a json formatted body and updates the prescription according to it
//
// Path: /api/patient/:num/visit/:visit/prescription/
//
// Params:
//   - num:   the id of the patient
//   - visit: the id of the visit
// Method: POST
func PostPrescription(ctx *gin.Context) {
	var visit = ctx.GetInt64("visit")
	var presc = ctx.MustGet("prescription").(*data.DbPrescriptionSummary)
	var exists, err = data.CheckMedicineExists(presc.MedNum)

	if err != nil {
		response.SendError(ctx, 500, fmt.Errorf("internal error: %w", err).Error())
		return
	}

	if !exists {
		response.SendError(ctx, 400, "bad request: no prescription found")
		return
	}

	presc.PreNum = preNode.Generate().Int64()
	presc.AptNum = visit
	err = data.InsertPrescription(*presc)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	ctx.JSON(200, presc)
}
