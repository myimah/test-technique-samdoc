package disease

import (
	"github.com/bwmarrin/snowflake"
	"github.com/gin-gonic/gin"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/doctor/docapi"
	"test-technique-samdoc/pkg/utils/response"
)

var disNode, _ = snowflake.NewNode(743)

//GetDiseases
// Endpoint that send a json formatted list of diseases.
//
// Path: /api/disease/
//
// Method: GET
var GetDiseases = docapi.GetAll(data.RetrieveAllDiseases)

//GetDisease
// Endpoint that send a json formatted disease
//
// Path: /api/disease/:dis
//
// Params:
//   - dis: the id of the disease
// Method: GET
var GetDisease = docapi.Get("dis", data.RetrieveDiseaseFromId)

//DeleteDisease
//Endpoint that deletes a disease from the database
//
// Path: /api/disease/:dis
//
// Params:
//   - dis: the id of the disease
// Method: DELETE
var DeleteDisease = docapi.Delete("dis", data.DeleteDisease)

//PostDisease
// Endpoint that receives a json formatted disease and stores it inside the database.
//
// Path: /api/disease/
//
// Method: POST
func PostDisease(ctx *gin.Context) {
	var disease = ctx.MustGet("disease").(*data.DbDisease)

	disease.Num = disNode.Generate().Int64()
	var err = data.InsertDisease(*disease)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	var summary = disease.DbDiseaseSummary
	ctx.JSON(200, summary)
}

//PutDisease
//Endpoint that receives a json formatted body and updates the disease according to it
//
//Path: /api/disease/:dis
//
// Params:
//   - dis: the id of the disease
// Method: PUT
func PutDisease(ctx *gin.Context) {
	var disease = &data.DbDisease{}
	var num = ctx.GetInt64("dis")
	var err = ctx.BindJSON(disease)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	disease.Num = num
	err = data.UpdateDisease(*disease)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	var newPatient, _ = data.RetrieveDiseaseFromId(num)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
		return
	}

	ctx.JSON(200, newPatient)
}
