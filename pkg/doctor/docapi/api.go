package docapi

import (
	"github.com/gin-gonic/gin"
	"test-technique-samdoc/pkg/utils/response"
)

//Get
//returns a function that handles the GET method
func Get(param string, retrieveFunc func(int64) (interface{}, error)) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		var num = ctx.GetInt64(param)
		var patient, err = retrieveFunc(num)

		if err != nil {
			response.SendError(ctx, 500, err.Error())
			return
		}

		ctx.JSON(200, patient)
	}
}

//GetAll
//returns a function that handles the GET method to retrieve all the elements
func GetAll(retrieveAllFunc func() (interface{}, error)) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		var values, err = retrieveAllFunc()

		if err != nil {
			response.SendError(ctx, 500, err.Error())
			return
		}

		ctx.JSON(200, values)
	}
}

type DeleteResponse struct {
	Message string `json:"message"`
}

//Delete
//returns a function that handles the DELETE method
func Delete(param string, deleteFunction func(int64) error) func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		var num = ctx.GetInt64(param)
		var err = deleteFunction(num)

		if err != nil {
			response.SendError(ctx, 500, err.Error())
			return
		}

		ctx.JSON(200, DeleteResponse{"Successfully removed"})
	}
}
