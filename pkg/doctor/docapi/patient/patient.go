package patient

import (
	"fmt"
	"github.com/bwmarrin/snowflake"
	"github.com/gin-gonic/gin"
	authdata "test-technique-samdoc/pkg/auth/data"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/doctor/docapi"
	"test-technique-samdoc/pkg/utils"
	"test-technique-samdoc/pkg/utils/response"
)

var patNode, _ = snowflake.NewNode(762)

//GetPatient
// Endpoint that send a json formatted patient
//
// Path: /api/patient/:num
//
// Params:
//   - num: the id of the patient
// Method: GET
var GetPatient = docapi.Get("num", data.RetrievePatientFromId)

//DeletePatient
//Endpoint that deletes a patient from the database
//
// Path: /api/patient/:pat
//
// Params:
//   - num: the id of the patient
// Method: DELETE
var DeletePatient = docapi.Delete("num", data.DeletePatient)

//GetPatients
// Endpoint that send a json formatted list of patient of a doctor.
//
// Only sends 3 fields : id, firstname, lastname
//
// Path: /api/patient/
//
// Method: GET
func GetPatients(ctx *gin.Context) {
	var token = ctx.MustGet("token").(*authdata.DbToken)
	docapi.GetAll(func() (interface{}, error) {
		return data.RetrievePatientsFromDoctorId(token.UserId)
	})(ctx)
}

//PostPatient
// Endpoint that receives a json formatted patient and stores it inside the database.
//
// Path: /api/patient/
//
// Method: POST
func PostPatient(ctx *gin.Context) {
	var token = ctx.MustGet("token").(*authdata.DbToken)
	var bodyPat = ctx.MustGet("patient").(*data.DbPatient)

	bodyPat.Id = patNode.Generate().Int64()
	bodyPat.DocId = token.UserId

	if !utils.DatetimeRegex.MatchString(bodyPat.Birthdate) {
		response.SendError(ctx, 400,
			fmt.Sprintf("invalid date format: %s, expected: yyyy-MM-dd", bodyPat.Birthdate))
		return
	}

	var err = data.InsertPatient(*bodyPat)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	var summary = bodyPat.DbPatientSummary
	ctx.JSON(200, summary)
}

//PutPatient
// Endpoint that receives a json formatted body and updates the patient according to it
//
// Path: /api/patient/:num
//
// Params:
//   - num: the id of the patient
//
// Method: PUT
func PutPatient(ctx *gin.Context) {
	var patient = &data.DbPatient{}
	var address = &data.DbAddress{}
	var num = ctx.GetInt64("num")
	var token = ctx.MustGet("token").(*authdata.DbToken)
	var err = ctx.BindJSON(address)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	patient.Id = num
	patient.DocId = token.UserId
	patient.DbAddress = *address

	if patient.Birthdate != "" && !utils.DatetimeRegex.MatchString(patient.Birthdate) {
		response.SendError(ctx, 400,
			fmt.Sprintf("invalid date format: %s, expected: yyyy-MM-dd", patient.Birthdate))
		return
	}

	err = data.UpdatePatient(*patient)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
	}

	var newPatient, _ = data.RetrievePatientFromId(num)

	if err != nil {
		response.SendError(ctx, 500, err.Error())
		return
	}

	ctx.JSON(200, newPatient)
}
