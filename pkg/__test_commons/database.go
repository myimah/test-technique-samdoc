package tc

import (
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	authdata "test-technique-samdoc/pkg/auth/data"
	"test-technique-samdoc/pkg/doctor/data"
)

var DbMock sqlmock.Sqlmock

func init() {
	var db *sql.DB
	db, DbMock, _ = sqlmock.New()
	authdata.InitWithDB(db)
	data.InitWithDB(db)
}
