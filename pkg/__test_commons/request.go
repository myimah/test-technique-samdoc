package tc

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http/httptest"
	"testing"
)

var WorkingToken = "a_great_test_token_that_will_not_work_in_production!!"
var WrongToken = "a_fake_token_that_does_not_exists!"

func toBase64(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

func CreateValidRequest(method, path string) (ctx *gin.Context, rec *httptest.ResponseRecorder) {
	rec = httptest.NewRecorder()
	ctx, _ = gin.CreateTestContext(rec)

	ctx.Request = httptest.NewRequest(method, path, nil)
	ctx.Request.Header.Set("authorization", fmt.Sprintf("Token %s", toBase64(WorkingToken)))

	if method == "PUT" || method == "POST" {
		ctx.Request.Header.Set("Content-Type", "application/json")
	}

	ctx.Request.Header.Set("Accept", "application/json")

	return
}

func CreateValidRequestWithBody(method, path string, body interface{}) (ctx *gin.Context, rec *httptest.ResponseRecorder) {
	rec = httptest.NewRecorder()
	ctx, _ = gin.CreateTestContext(rec)

	var bodyJson []byte
	_ = json.Unmarshal(bodyJson, body)

	var bBuffer = new(bytes.Buffer)
	bBuffer.Write(bodyJson)

	ctx.Request = httptest.NewRequest(method, path, bBuffer)
	ctx.Request.Header.Set("authorization", fmt.Sprintf("Token %s", toBase64(WorkingToken)))
	ctx.Request.Header.Set("Content-Type", "application/json")
	ctx.Request.Header.Set("Accept", "application/json")

	return
}

func ParseBody(t *testing.T, buffer *bytes.Buffer, target interface{}) {
	var err = json.Unmarshal(buffer.Bytes(), target)

	if err != nil {
		t.Fatalf("failed to parse body : %s", err)
	}
}
