package authdata

import (
	"database/sql"
	"fmt"
	"github.com/bwmarrin/snowflake"
	"github.com/thanhpk/randstr"
)

//DbUser
// The representation of a user from the database.
type DbUser struct {
	Id       int64  `json:"usr_id"`
	Username string `json:"usr_username"`
}

var snNode, _ = snowflake.NewNode(654)

func parseUser(row *sql.Row) (*DbUser, error) {
	var user = &DbUser{}
	var err = row.Scan(&user.Id, &user.Username)

	if err == sql.ErrNoRows {
		return nil, ErrNoUserFound
	}

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve user: %w", err)
	}

	return user, nil
}

//CheckUserExists
//Returns whether the username exists inside the database
func CheckUserExists(username string) (bool, error) {
	var q, err = db.Prepare("SELECT COUNT(*) AS user_count FROM AUTH_USER WHERE usr_username = ?")

	if err != nil {
		return false, fmt.Errorf("failed to retrieve user: %w", err)
	}
	var row = q.QueryRow(username)
	var userCount = 0

	_ = row.Scan(&userCount)
	return userCount != 0, nil
}

//RetrieveUserFromId
//Returns the user from the id.
func RetrieveUserFromId(id int64) (*DbUser, error) {
	var q, err = db.Prepare("SELECT usr_id, usr_username FROM AUTH_USER WHERE usr_id = ?")

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve user: %w", err)
	}

	var row = q.QueryRow(id)
	return parseUser(row)
}

//RetrieveUserFromCredentials
//Returns the user from a pair of username and password.
func RetrieveUserFromCredentials(username, password string) (*DbUser, error) {
	var q, err = db.Prepare("SELECT usr_id, usr_username FROM AUTH_USER WHERE usr_username = ? AND usr_password = SHA2(CONCAT(?, usr_salt), 256)")

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve user: %w", err)
	}

	var row = q.QueryRow(username, password)
	return parseUser(row)
}

//CreateNewUser
//Creates a new user inside the database and returns his id.
func CreateNewUser(username, password string) (int64, error) {
	var exists, err = CheckUserExists(username)

	if err != nil {
		return -1, fmt.Errorf("failed to create user: %w", err)
	}

	if exists {
		return -1, ErrUsernameUsed
	}

	var salt = randstr.String(10)
	var id = snNode.Generate().Int64()

	var q, _ = db.Prepare("INSERT INTO AUTH_USER (USR_ID, USR_USERNAME, USR_PASSWORD, USR_SALT) " +
		"VALUES (?, ?, SHA2(?, 256), ?)")

	_, err = q.Exec(id, username, password+salt, salt)

	if err != nil {
		return 0, fmt.Errorf("failed to create user: %w", err)
	}

	return id, nil
}
