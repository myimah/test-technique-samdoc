package authdata

import (
	"database/sql"
	"errors"
	"fmt"
	"test-technique-samdoc/pkg/utils/config"
	"test-technique-samdoc/pkg/utils/database"
)

var db *sql.DB

// ErrNoUserFound
// happens when the pair of user and password is not valid
var ErrNoUserFound = errors.New("invalid user or password")

//ErrUsernameUsed
// happens when trying to create a new user with an already registered username
var ErrUsernameUsed = errors.New("username already in use")

//ErrNoTokenFound
// happens when the used token is not valid
var ErrNoTokenFound = errors.New("invalid token")

//Init configuration
//Initialize the database for the auth server
func Init(configuration config.DbConfig) error {
	var err error
	db, err = database.CreateConnection(configuration)

	if err != nil {
		return fmt.Errorf("failed to intialize the connection to the database: %w", err)
	}

	return err
}

func InitWithDB(dbConn *sql.DB) {
	db = dbConn
}
