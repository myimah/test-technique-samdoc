package authdata

import (
	"database/sql"
	"encoding/base64"
	"fmt"
	"github.com/thanhpk/randstr"
)

//DbToken
// The representation of a token from the database.
type DbToken struct {
	Token      string `json:"tok_token"`
	Expiration string `json:"tok_expiration"`
	UserId     int64  `json:"usr_id"`
}

//RetrieveAuthUser
// Returns the user liked by this token
func (t DbToken) RetrieveAuthUser() (*DbUser, error) {
	return RetrieveUserFromId(t.UserId)
}

func parseToken(row *sql.Row) (*DbToken, error) {
	var token = &DbToken{}
	var err = row.Scan(&token.Token, &token.UserId, &token.Expiration)

	if err == sql.ErrNoRows {
		return nil, ErrNoTokenFound
	}

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve token2: %w", err)
	}

	token.Token = base64.StdEncoding.EncodeToString([]byte(token.Token))

	return token, nil
}

//RetrieveTokenObject token
// Returns the DbToken structure of the given token
func RetrieveTokenObject(token string) (*DbToken, error) {
	var q, err = db.Prepare("SELECT * FROM AUTH_TOKEN WHERE tok_token = ? AND tok_expiration > SYSDATE()")

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve token1: %w", err)
	}

	var row = q.QueryRow(token)
	return parseToken(row)
}

func (u *DbUser) getLastToken() (*DbToken, error) {
	var q, err = db.Prepare("SELECT * FROM AUTH_TOKEN WHERE tok_expiration > SYSDATE() AND usr_id = ? LIMIT 1")

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve token: %w", err)
	}

	var row = q.QueryRow(u.Id)
	return parseToken(row)
}

func (u *DbUser) generateToken() (*DbToken, error) {
	var token = randstr.String(128)
	var q, err = db.Prepare("INSERT INTO AUTH_TOKEN (tok_token, usr_id) VALUES (?, ?)")

	if err != nil {
		return nil, fmt.Errorf("failed to generate token: %w", err)
	}

	_, err = q.Exec(token, u.Id)

	if err != nil {
		return nil, fmt.Errorf("failed to generate token: %w", err)
	}

	return RetrieveTokenObject(token)
}

//GetOrGenerateToken
// returns the current token inside the database or generate if the last one is expired.
func (u *DbUser) GetOrGenerateToken() (*DbToken, error) {
	var tok, err = u.getLastToken()

	if err != nil {
		if err == ErrNoTokenFound {
			return u.generateToken()
		}
		return nil, err
	}

	return tok, nil
}
