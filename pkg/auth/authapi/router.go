package authapi

import (
	"github.com/gin-gonic/gin"
	"log"
	"os"
	"reflect"
	"test-technique-samdoc/pkg/auth/data"
	"test-technique-samdoc/pkg/utils/middleware"
	"test-technique-samdoc/pkg/utils/middleware/validator"
	"test-technique-samdoc/pkg/utils/response"
)

//AuthBody the body sent by the client to authenticate
// Username The username of the client
// Password The password of the client
type AuthBody struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}

var logger = log.New(os.Stdout, "[HTTP-AUTH]", log.Flags())

var LoginValidator = validator.NewValidator("creds", reflect.TypeOf(AuthBody{}))

//Route engine
// Adds the engine the different routes used for the authentication server
func Route(engine *gin.Engine) {
	engine.Use(middleware.CheckContentType)
	engine.POST("/auth/login", LoginValidator, LoginEndpoint)
}

//LoginEndpoint ctx
// An endpoint that gets a AuthBody json formatted and gives back an api token.
// Path: /auth/login
// Method: POST
func LoginEndpoint(ctx *gin.Context) {
	var authBody = ctx.MustGet("creds").(*AuthBody)

	var user, err = authdata.RetrieveUserFromCredentials(authBody.Username, authBody.Password)

	if err != nil {
		response.SendError(ctx, 401, err.Error())

		if err != authdata.ErrNoUserFound {
			logger.Println(err)
		}
		return
	}

	var token *authdata.DbToken
	token, err = user.GetOrGenerateToken()

	if err != nil {
		response.SendError(ctx, 401, err.Error())
		logger.Println(err)
		return
	}

	ctx.JSON(200, token)
}
