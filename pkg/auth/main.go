package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"test-technique-samdoc/pkg/auth/authapi"
	"test-technique-samdoc/pkg/auth/data"
	"test-technique-samdoc/pkg/utils/config"
)

func main() {
	var conf, err = config.LoadConfig("dev_config.json")
	if err != nil {
		fmt.Println(err)
	}
	_ = authdata.Init(conf.AuthDB)

	var engine = gin.Default()
	authapi.Route(engine)
	err = engine.Run(":8080")
	if err != nil {
		var errfmt = fmt.Errorf("error while running server: %w", err)
		fmt.Println(errfmt)
	}
}
