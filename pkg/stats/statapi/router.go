package statapi

import (
	"fmt"
	"github.com/gin-gonic/gin"
	statdata "test-technique-samdoc/pkg/stats/data"
	"test-technique-samdoc/pkg/utils/middleware"
	"test-technique-samdoc/pkg/utils/response"
)

var statHandlers = map[string]gin.HandlerFunc{
	"pat-per-dis": PrepareSendStats(statdata.GetPatientPerDisease),
}

//Route engine
// Adds the engine the different routes used for the stats server
func Route(engine *gin.Engine) {
	engine.Use(middleware.CheckContentType)
	engine.Use(middleware.CheckAuth)

	var statGroup = engine.Group("/stats/")
	{
		statGroup.GET("/:statname", sendStats)
	}
}

//PrepareSendStats
//Returns an handler which will retrieve the data and send it to the client
func PrepareSendStats(retrieveFunc func() (interface{}, error)) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var data, err = retrieveFunc()

		if err != nil {
			response.SendError(ctx, 500, fmt.Errorf("internal error: %w", err).Error())
			return
		}

		ctx.JSON(200, data)
	}
}

func sendStats(ctx *gin.Context) {
	var stat = ctx.Param("statname")
	if handler, in := statHandlers[stat]; in {
		handler(ctx)
	} else {
		response.SendError(ctx, 404, "stat not found")
	}
}
