package data

import (
	"fmt"
)

//PatPerDis
//A structure holding a disease and its number of patient who had had this disease
type PatPerDis struct {
	DisNum       int64  `json:"disease_num"`
	DisLabel     string `json:"disease_label"`
	NumOfPatient int    `json:"num_of_patient"`
}

//GetPatientPerDisease
//returns a list of PatPerDis
func GetPatientPerDisease() (interface{}, error) {
	var q, err = db.Query("SELECT * FROM SD_V_NUM_PATIENT_PER_DISEASE WHERE NUMBER > 0")

	if err != nil {
		return nil, fmt.Errorf("failed to retrieve stats: %w", err)
	}

	var stats = make([]PatPerDis, 0)

	for q.Next() {
		var row PatPerDis

		err = q.Scan(&row.DisNum, &row.DisLabel, &row.NumOfPatient)

		if err != nil {
			return nil, fmt.Errorf("failed to retrieve stats: %w", err)
		}

		stats = append(stats, row)
	}

	return stats, nil
}
