package data

import (
	"database/sql"
	"fmt"
	"test-technique-samdoc/pkg/utils/config"
	"test-technique-samdoc/pkg/utils/database"
)

var db *sql.DB

//Init configuration
//Initialize the database for the stat server
func Init(configuration config.DbConfig) error {
	var err error
	db, err = database.CreateConnection(configuration)

	if err != nil {
		return fmt.Errorf("failed to intialize the connection to the database: %w", err)
	}

	return err
}

func InitWithDB(dbConn *sql.DB) {
	db = dbConn
}
