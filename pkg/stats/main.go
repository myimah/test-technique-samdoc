package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	authdata "test-technique-samdoc/pkg/auth/data"
	docdata "test-technique-samdoc/pkg/doctor/data"
	statdata "test-technique-samdoc/pkg/stats/data"
	"test-technique-samdoc/pkg/stats/statapi"
	"test-technique-samdoc/pkg/utils/config"
)

func main() {
	var conf, err = config.LoadConfig("dev_config.json")

	if err != nil {
		fmt.Println(err)
	}
	_ = docdata.Init(conf.MainDB)
	_ = authdata.Init(conf.AuthDB)
	statdata.InitWithDB(docdata.GetDB())

	var engine = gin.Default()
	statapi.Route(engine)
	err = engine.Run(":8082")
	if err != nil {
		var errfmt = fmt.Errorf("error while running server: %w", err)
		fmt.Println(errfmt)
	}
}
