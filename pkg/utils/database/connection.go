package database

import (
	"database/sql"
	"fmt"
	"test-technique-samdoc/pkg/utils/config"
)
import _ "github.com/go-sql-driver/mysql"

//CreateConnection
// Creates a new connection to the database from the given configuration.
func CreateConnection(configuration config.DbConfig) (*sql.DB, error) {
	var dataSource = fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", configuration.User, configuration.Pass,
		configuration.Host, configuration.Name)
	db, err := sql.Open("mysql", dataSource)

	if err != nil {
		return nil, fmt.Errorf("failed to connect to database: %w", err)
	}
	return db, nil
}
