package middleware_test

import (
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/go-playground/assert/v2"
	tc "test-technique-samdoc/pkg/__test_commons"
	"test-technique-samdoc/pkg/utils/middleware"
	"testing"
)

func TestCheckDiagnosticOwnership__valid(t *testing.T) {
	var ctx, rec = tc.CreateValidRequest("DELETE", "/")
	ctx.Set("visit", int64(1234))
	ctx.Set("dis", int64(1234))
	rec.Code = 0

	tc.DbMock.ExpectPrepare(`SELECT COUNT\(\*\) AS c FROM SD_DIAGNOSE WHERE APT_NUM = \? AND DIS_NUM = \?`).
		ExpectQuery().
		WithArgs(1234, 1234).
		WillReturnRows(sqlmock.NewRows([]string{"c"}).AddRow(1))

	middleware.CheckDiagnosticOwnership(ctx)

	assert.Equal(t, rec.Code, 0)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}
}

func TestCheckDiagnosticOwnership__notFound(t *testing.T) {
	var ctx, rec = tc.CreateValidRequest("DELETE", "/")
	ctx.Set("visit", int64(1234))
	ctx.Set("dis", int64(1234))
	rec.Code = 0

	tc.DbMock.ExpectPrepare(`SELECT COUNT\(\*\) AS c FROM SD_DIAGNOSE WHERE APT_NUM = \? AND DIS_NUM = \?`).
		ExpectQuery().
		WithArgs(1234, 1234).
		WillReturnRows(sqlmock.NewRows([]string{"c"}).AddRow(0))

	middleware.CheckDiagnosticOwnership(ctx)

	assert.Equal(t, rec.Code, 404)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}
}

func TestCheckDiagnosticOwnership__internalError(t *testing.T) {
	var ctx, rec = tc.CreateValidRequest("DELETE", "/")
	ctx.Set("visit", int64(1234))
	ctx.Set("dis", int64(1234))
	rec.Code = 0

	tc.DbMock.ExpectPrepare(`SELECT COUNT\(\*\) AS c FROM SD_DIAGNOSE WHERE APT_NUM = \? AND DIS_NUM = \?`).
		WillReturnError(errors.New("fake internal error"))

	middleware.CheckDiagnosticOwnership(ctx)

	assert.Equal(t, rec.Code, 500)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}
}
