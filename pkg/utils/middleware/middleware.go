package middleware

import (
	"encoding/base64"
	"fmt"
	"github.com/gin-gonic/gin"
	"strings"
	authdata "test-technique-samdoc/pkg/auth/data"
	"test-technique-samdoc/pkg/utils/response"
)

//CheckContentType
//Checks if the Content-Type header is "application/json". If not sends a 400
func CheckContentType(ctx *gin.Context) {
	var doesNeedBody = ctx.Request.Method == "PUT" || ctx.Request.Method == "POST"
	ctx.Header("Content-Type", "application/json;utf-8")

	if doesNeedBody && !strings.Contains(ctx.ContentType(), "application/json") {
		response.SendError(ctx, 400, `Bad Request: Content-Type header must be set to "application/json".`)
	}
}

//CheckAuth
//A middleware to check if the client is authenticated. If not sends a 401
func CheckAuth(ctx *gin.Context) {
	var authorization = strings.TrimSpace(ctx.GetHeader("authorization"))
	var err error

	if authorization != "" && strings.HasPrefix(authorization, "Token") {
		var tokenBytes []byte
		tokenBytes, err = base64.StdEncoding.DecodeString(strings.TrimSpace(authorization[5:]))

		if err == nil {
			var token, err = authdata.RetrieveTokenObject(string(tokenBytes))

			if err != nil {
				if err == authdata.ErrNoTokenFound {
					response.SendError(ctx, 401, fmt.Sprintf(`Unauthorized: %s`, err.Error()))
				} else {
					response.SendError(ctx, 500, fmt.Sprintf(`Internal Error: %s`, err.Error()))
				}
			} else {
				ctx.Set("token", token)
			}
			return
		}
	}

	response.SendError(ctx, 400, `Bad Request: Missing or malformed "Authorization" header`)
}
