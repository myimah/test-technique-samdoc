package validator

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"reflect"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/doctor/docapi/doctor"
	"test-technique-samdoc/pkg/utils/response"
)

//InsertVisitValidator
//Check if the struct data.DbVisit validates the body
var InsertVisitValidator = NewValidator("visit", reflect.TypeOf(data.DbVisit{}))

//InsertPatientValidator
//Check if the struct data.DbPatient validates the body
var InsertPatientValidator = NewValidator("patient", reflect.TypeOf(data.DbPatient{}))

//InsertDiseaseValidator
//Check if the struct data.DbDisease validates the body
var InsertDiseaseValidator = NewValidator("disease", reflect.TypeOf(data.DbDisease{}))

//InsertMedicineValidator
//Check if the struct data.DbMedicine validates the body
var InsertMedicineValidator = NewValidator("medicine", reflect.TypeOf(data.DbMedicine{}))

//InsertPrescriptionValidator
//Check if the struct data.DbPrescriptionSummary validates the body
var InsertPrescriptionValidator = NewValidator("prescription", reflect.TypeOf(data.DbPrescriptionSummary{}))

//InsertDiagnosticValidator
//Check if the struct data.DbDiagnostic validates the body
var InsertDiagnosticValidator = NewValidator("diagnostic", reflect.TypeOf(data.DbDiagnostic{}))

//RegisterDoctorValidator
//Check if the struct doctor.RegisterBody validates the body
var RegisterDoctorValidator = NewValidator("account", reflect.TypeOf(doctor.RegisterBody{}))

//NewValidator
//Returns a function that will check if the structure validates the body
//
//If not, sends bad request
func NewValidator(field string, valType reflect.Type) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var body = reflect.New(valType).Interface()
		var err = ctx.ShouldBindJSON(body)

		if err != nil {
			response.SendError(ctx, 500, err.Error())
			return
		}
		var validate = validator.New()

		err = validate.Struct(body)

		if err != nil {
			response.SendError(ctx, 400, fmt.Sprintf("Bad Request: %s", err.Error()))
			return
		}

		ctx.Set(field, body)
	}
}
