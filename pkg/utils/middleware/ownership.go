package middleware

import (
	"fmt"
	"github.com/gin-gonic/gin"
	authdata "test-technique-samdoc/pkg/auth/data"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/utils/response"
)

//CheckPatientOwnership
//Checks whether the client has access to the patient information. If not sends a 401
func CheckPatientOwnership(ctx *gin.Context) {
	var pNum = ctx.GetInt64("num")
	var token = ctx.MustGet("token").(*authdata.DbToken)

	if pNum != 0 {
		var ownership, err = data.CheckPatientOwnership(token.UserId, pNum)

		if err != nil {
			response.SendError(ctx, 500, fmt.Sprintf(`Internal Error: %s`, err.Error()))
			return
		}

		if !ownership {
			response.SendError(ctx, 401, `Unauthorized`)
		}
	} else {
		// Should not reach this error.
		response.SendError(ctx, 400, `Bad Request: Missing URL parameter "num"`)
	}
}

//CheckVisitOwnership
//A middleware that checks if the visit is assigned to the patient. If not sends a 404
func CheckVisitOwnership(ctx *gin.Context) {
	var pNum = ctx.GetInt64("num")
	var vNum = ctx.GetInt64("visit")

	var ownership, err = data.CheckVisitOwnership(pNum, vNum)

	if err != nil {
		response.SendError(ctx, 500, fmt.Sprintf(`Internal Error: %s`, err.Error()))
		return
	}

	if !ownership {
		response.SendError(ctx, 404, `Not Found`)
	}
}

//CheckPrescriptionOwnership
//A middleware that checks if the prescription is assigned to a patient. If not sends a 404
func CheckPrescriptionOwnership(ctx *gin.Context) {
	var patId = ctx.GetInt64("num")
	var preNum = ctx.GetInt64("presc")

	var ownership, err = data.CheckPrescriptionOwnership(preNum, patId)

	if err != nil {
		response.SendError(ctx, 500, fmt.Sprintf(`Internal Error: %s`, err.Error()))
		return
	}

	if !ownership {
		response.SendError(ctx, 404, `Not Found`)
	}
}

//CheckDiagnosticOwnership
//A middleware that checks if the disease is assigned to a visit. If not sends a 404
func CheckDiagnosticOwnership(ctx *gin.Context) {
	var disId = ctx.GetInt64("dis")
	var visitId = ctx.GetInt64("visit")

	var ownership, err = data.CheckDiagnosticOwnership(disId, visitId)

	if err != nil {
		response.SendError(ctx, 500, fmt.Sprintf(`Internal Error: %s`, err.Error()))
		return
	}

	if !ownership {
		response.SendError(ctx, 404, `Not Found`)
	}
}
