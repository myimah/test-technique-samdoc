package middleware_test

import (
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"net/http/httptest"
	tc "test-technique-samdoc/pkg/__test_commons"
	"test-technique-samdoc/pkg/utils/middleware"
	"testing"
)

func toBase64(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

func TestCheckAuth__valid(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Request = httptest.NewRequest("GET", "/", nil)
	ctx.Request.Header.Set("authorization", fmt.Sprintf("Token %s", toBase64(tc.WorkingToken)))

	rec.Code = 0 // Should not change

	var columns = []string{"TOK_TOKEN", "USR_ID", "TOK_EXPIRATION"}

	tc.DbMock.
		ExpectPrepare(`SELECT \* FROM AUTH_TOKEN WHERE tok_token = \? AND tok_expiration > SYSDATE\(\)`).
		ExpectQuery().
		WithArgs(tc.WorkingToken).
		WillReturnRows(sqlmock.NewRows(columns).AddRow(tc.WorkingToken, 78523156456435123,
			"2800-12-31 23:59:59"))

	middleware.CheckAuth(ctx)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}

	assert.Equal(t, rec.Code, 0)
}

func TestCheckAuth__invalid(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Request = httptest.NewRequest("GET", "/", nil)
	ctx.Request.Header.Set("authorization", fmt.Sprintf("Token %s", toBase64(tc.WrongToken)))

	rec.Code = 0 // Should change

	tc.DbMock.
		ExpectPrepare(`SELECT \* FROM AUTH_TOKEN WHERE tok_token = \? AND tok_expiration > SYSDATE\(\)`).
		ExpectQuery().
		WithArgs(tc.WrongToken).
		WillReturnError(sql.ErrNoRows)

	middleware.CheckAuth(ctx)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}

	assert.Equal(t, rec.Code, 401)
}

func TestCheckAuth__internalError(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Request = httptest.NewRequest("GET", "/", nil)
	ctx.Request.Header.Set("authorization", fmt.Sprintf("Token %s", toBase64(tc.WrongToken)))

	rec.Code = 0 // Should change

	tc.DbMock.
		ExpectPrepare(`SELECT \* FROM AUTH_TOKEN WHERE tok_token = \? AND tok_expiration > SYSDATE\(\)`).
		ExpectQuery().
		WithArgs(tc.WrongToken).
		WillReturnError(errors.New("fake internal error"))

	middleware.CheckAuth(ctx)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}

	assert.Equal(t, rec.Code, 500)
}

func TestCheckAuth__badRequest(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Request = httptest.NewRequest("GET", "/", nil)
	ctx.Request.Header.Set("authorization", "I have the right!")

	rec.Code = 0 // Should change

	middleware.CheckAuth(ctx)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}

	assert.Equal(t, rec.Code, 400)
}

func TestContentTypeMiddleware__valid__whenNeeded(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Request = httptest.NewRequest("POST", "/", nil)
	ctx.Request.Header.Set("Content-Type", "application/json")
	ctx.Request.Header.Set("Accept", "application/json")

	rec.Code = 0 // should not change

	middleware.CheckContentType(ctx)

	assert.Equal(t, rec.Code, 0)
}

func TestContentTypeMiddleware__valid__whenNotNeeded(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Request = httptest.NewRequest("GET", "/", nil)
	ctx.Request.Header.Set("Accept", "application/json")

	ctx.Set("t", t)

	rec.Code = 0 // should not change

	middleware.CheckContentType(ctx)

	assert.Equal(t, rec.Code, 0)
}

func TestContentTypeMiddleware__MissingContentTypeWhenNeeded(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Request = httptest.NewRequest("POST", "/", nil)
	ctx.Request.Header.Set("Accept", "application/json")

	rec.Code = 0 // should change

	middleware.CheckContentType(ctx)

	assert.Equal(t, rec.Code, 400)
}

func TestContentTypeMiddleware__MissingContentTypeWhenNotNeeded(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Request = httptest.NewRequest("POST", "/", nil)
	ctx.Request.Header.Set("Accept", "application/json")

	rec.Code = 0 // should change

	middleware.CheckContentType(ctx)

	assert.Equal(t, rec.Code, 400)
}
