package middleware

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/utils/response"
)

var CheckPatientExists = CheckExists("num", "patient", data.CheckPatientExists)
var CheckVisitExists = CheckExists("visit", "visit", data.CheckVisitExists)
var CheckMedicineExists = CheckExists("med", "medicine", data.CheckMedicineExists)
var CheckDiseaseExists = CheckExists("dis", "disease", data.CheckDiseaseExists)
var CheckPrescriptionExists = CheckExists("presc", "prescription", data.CheckPrescriptionExists)

//CheckExists
//Checks if the paramName is set and that checkFunction validates its value.
func CheckExists(paramName string, dataType string, checkFunction func(int64) (bool, error)) func(*gin.Context) {
	return func(ctx *gin.Context) {
		var param = ctx.Param(paramName)

		if param != "" {
			var err error
			var num int64

			num, err = strconv.ParseInt(param, 10, 64)

			if err != nil {
				response.SendError(ctx, 400, fmt.Sprintf(`Bad Request: Invalid format of %s id.`, paramName))
				return
			}

			var exists bool

			exists, err = checkFunction(num)

			if err != nil {
				response.SendError(ctx, 500, fmt.Sprintf(`Internal Error: %s`, err.Error()))
				return
			}

			if !exists {
				response.SendError(ctx, 404, fmt.Sprintf("no %s found with id %d", dataType, num))
				return
			}

			ctx.Set(paramName, num)
		} else {
			// Should not reach this error.
			response.SendError(ctx, 400, fmt.Sprintf(`Bad Request: Missing URL parameter "%s"`, paramName))

		}
	}
}
