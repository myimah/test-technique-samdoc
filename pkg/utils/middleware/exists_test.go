package middleware_test

import (
	"database/sql"
	"errors"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"net/http/httptest"
	tc "test-technique-samdoc/pkg/__test_commons"
	"test-technique-samdoc/pkg/doctor/data"
	"test-technique-samdoc/pkg/utils/middleware"
	"testing"
)

var CheckExistsImpl = middleware.CheckExists("test", "test",
	data.CheckExists("TEST", "TEST", "test"))

func TestCheckExists__valid(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Params = append(ctx.Params, gin.Param{Key: "test", Value: "123456789"})
	rec.Code = 0

	tc.DbMock.ExpectPrepare(`SELECT COUNT\(\*\) AS c FROM TEST WHERE TEST = \?`).
		ExpectQuery().
		WithArgs(123456789).
		WillReturnRows(sqlmock.NewRows([]string{"c"}).AddRow(1))

	CheckExistsImpl(ctx)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}

	assert.Equal(t, rec.Code, 0)
}

func TestCheckExists__invalid__wrongFormat(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Params = append(ctx.Params, gin.Param{Key: "test", Value: "hehe"})
	rec.Code = 0

	CheckExistsImpl(ctx)

	assert.Equal(t, rec.Code, 400)
}

func TestCheckExists__invalid__notFound(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Params = append(ctx.Params, gin.Param{Key: "test", Value: "9999999"})
	rec.Code = 0

	tc.DbMock.ExpectPrepare(`SELECT COUNT\(\*\) AS c FROM TEST WHERE TEST = \?`).
		ExpectQuery().
		WithArgs(9999999).
		WillReturnError(sql.ErrNoRows)

	CheckExistsImpl(ctx)

	if err := tc.DbMock.ExpectationsWereMet(); err != nil {
		t.Fatalf("unfulfilled expectations: %s", err)
	}

	assert.Equal(t, rec.Code, 404)
}

func TestCheckExists__invalid__internalError(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	ctx.Params = append(ctx.Params, gin.Param{Key: "test", Value: "666"})
	rec.Code = 0

	tc.DbMock.ExpectPrepare(`SELECT COUNT\(\*\) AS c FROM TEST WHERE TEST = \?`).
		ExpectQuery().
		WithArgs(666).
		WillReturnError(errors.New("an internal error"))

	CheckExistsImpl(ctx)

	assert.Equal(t, rec.Code, 500)
}

func TestCheckExists__invalid__missingParameter(t *testing.T) {
	var rec = httptest.NewRecorder()
	var ctx, _ = gin.CreateTestContext(rec)
	rec.Code = 0

	CheckExistsImpl(ctx)

	assert.Equal(t, rec.Code, 400)
}
