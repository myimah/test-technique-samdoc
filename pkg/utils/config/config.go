package config

//DbConfig
//A representation of the configuration file
//Host the host of the database
//Name the name of the database
//User the username to connect to the database
//Pass the password to connect to the database
type DbConfig struct {
	Host string `json:"DB_HOST"`
	Name string `json:"DB_NAME"`
	User string `json:"DB_USER"`
	Pass string `json:"DB_PASS"`
}

type Configuration struct {
	MainDB DbConfig `json:"main"`
	AuthDB DbConfig `json:"auth"`
}
