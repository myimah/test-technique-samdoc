package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

//LoadConfig
// Loads the configuration file and returns its content
func LoadConfig(filename string) (*Configuration, error) {
	var fileContent, err = ioutil.ReadFile(filename)
	var configuration = &Configuration{}

	if err != nil {
		return nil, fmt.Errorf("couldn't load config file: %w", err)
	}

	err = json.Unmarshal(fileContent, configuration)

	if err != nil {
		return nil, fmt.Errorf("json error while reading config file: %w", err)
	}

	return configuration, nil
}
