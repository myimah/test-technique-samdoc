package utils

import (
	"regexp"
)

var DatetimeRegex = regexp.MustCompile(`^\d{4}-\d\d-\d\d( \d\d:\d\d:\d\d(.\d+)?(([+-]\d\d:\d\d)|Z)?)?$`)
