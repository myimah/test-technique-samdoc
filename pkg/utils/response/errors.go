package response

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

//ErrorResponse a representation of the error response
// Code the error code
// Message a message that explains what happened
type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// SendError
// Sends the error message to the client.
func SendError(ctx *gin.Context, code int, message string) {
	ctx.JSON(code, ErrorResponse{
		code,
		message,
	})

	if int(code/100) == 5 {
		fmt.Println(message)
	}

	ctx.Abort()
}
