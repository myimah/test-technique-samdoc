module test-technique-samdoc

go 1.17

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/bwmarrin/snowflake v0.3.0
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/assert/v2 v2.0.1
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/thanhpk/randstr v1.0.4
)
