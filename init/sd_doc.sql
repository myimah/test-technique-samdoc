-- MySQL dump 10.19  Distrib 10.3.32-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sd_doc
-- ------------------------------------------------------
-- Server version	10.5.13-MariaDB-0ubuntu0.21.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `SD_APPOINTMENT`
--

DROP TABLE IF EXISTS `SD_APPOINTMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SD_APPOINTMENT` (
  `APT_NUM` bigint(4) NOT NULL,
  `PAT_ID` bigint(4) NOT NULL,
  `APT_DATE` datetime NOT NULL,
  `APT_REASON` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`APT_NUM`),
  KEY `FK_SD_APPOINTMENT_SD_PATIENT` (`PAT_ID`),
  CONSTRAINT `FK_SD_APPOINTMENT_SD_PATIENT` FOREIGN KEY (`PAT_ID`) REFERENCES `SD_PATIENT` (`PAT_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SD_APPOINTMENT`
--

LOCK TABLES `SD_APPOINTMENT` WRITE;
/*!40000 ALTER TABLE `SD_APPOINTMENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `SD_APPOINTMENT` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER trig_sd_appointment_update
    BEFORE UPDATE
    ON SD_APPOINTMENT
    FOR EACH ROW
BEGIN
    IF NEW.APT_REASON = '' THEN
        SET NEW.APT_REASON = OLD.APT_REASON;
    END IF;

    IF NEW.APT_DATE = '' THEN
        SET NEW.APT_DATE = OLD.APT_DATE;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `SD_DIAGNOSE`
--

DROP TABLE IF EXISTS `SD_DIAGNOSE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SD_DIAGNOSE` (
  `APT_NUM` bigint(4) NOT NULL,
  `DIS_NUM` bigint(4) NOT NULL,
  PRIMARY KEY (`APT_NUM`,`DIS_NUM`),
  KEY `FK_SD_DIAGNOSE_SD_DISEASE` (`DIS_NUM`),
  CONSTRAINT `FK_SD_DIAGNOSE_SD_APPOINTMENT` FOREIGN KEY (`APT_NUM`) REFERENCES `SD_APPOINTMENT` (`APT_NUM`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SD_DIAGNOSE_SD_DISEASE` FOREIGN KEY (`DIS_NUM`) REFERENCES `SD_DISEASE` (`DIS_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SD_DIAGNOSE`
--

LOCK TABLES `SD_DIAGNOSE` WRITE;
/*!40000 ALTER TABLE `SD_DIAGNOSE` DISABLE KEYS */;
/*!40000 ALTER TABLE `SD_DIAGNOSE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SD_DISEASE`
--

DROP TABLE IF EXISTS `SD_DISEASE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SD_DISEASE` (
  `DIS_NUM` bigint(4) NOT NULL,
  `DIS_LABEL` varchar(128) NOT NULL,
  PRIMARY KEY (`DIS_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SD_DISEASE`
--

LOCK TABLES `SD_DISEASE` WRITE;
/*!40000 ALTER TABLE `SD_DISEASE` DISABLE KEYS */;
INSERT INTO `SD_DISEASE` VALUES (1483552655114399744,'Actinomycose'),(1483552655114399745,'Acinetobacter'),(1483552657698091008,'Maladie du charbon'),(1483552657698091009,'Arcanobacterium haemolyticum'),(1483552657974915072,'SIDA'),(1483552658096549888,'Fièvre hémorragique d\'Argentine'),(1483552659824603136,'Aspergillose'),(1483552659853963264,'Astrovirus'),(1483552659870740480,'Ascaridiose'),(1483552661292609536,'Babésiose'),(1483552661410050048,'Bacillus cereus'),(1483552663263932416,'Maladie du sommeil'),(1483552663280709632,'Anaplasmose'),(1483552663360401408,'Amibiase'),(1483552663360401409,'Pneumonie bactérienne'),(1483552664832602112,'Bacteroides'),(1483552664975208448,'Balantidiase'),(1483552665335918592,'Baylisascaris'),(1483552665348501504,'Virus BK'),(1483552665730183168,'Piedra noire'),(1483552665746960384,'Blastocystis hominis'),(1483552666267054080,'Blastomycose'),(1483552667596648448,'Bartonellose'),(1483552667995107328,'Fièvre hémorragique bolivienne'),(1483552667995107329,'Borréliose'),(1483552668376788992,'Botulisme'),(1483552668435509248,'Fièvre hémorragique brésilienne'),(1483552668787830784,'Bronchiolites'),(1483552668875911168,'Brucellose'),(1483552669370839040,'Ulcère de Buruli'),(1483552669391810560,'Burkholderia'),(1483552669874155520,'Calicivirus'),(1483552670004178944,'Campylobactériose'),(1483552670503301120,'Candidiase'),(1483552670578798592,'Maladie des griffes du chat'),(1483552671090503680,'Maladie de Carrion'),(1483552671098892288,'Cellulite'),(1483552672826945536,'Maladie de Chagas'),(1483552672885665792,'Varicelle'),(1483552673426731008,'Capnocytophaga'),(1483552673443508224,'Chancre mou'),(1483552674097819648,'Chikungunya'),(1483552674122985472,'Chlamydophila pneumoniae'),(1483552674676633600,'Choléra'),(1483552674697605120,'Clonorchiase'),(1483552675062509568,'Colite hémorragique'),(1483552675150589952,'Cryptococcose'),(1483552675549048832,'Clostridium difficile'),(1483552675570020352,'Cytomégalovirus'),(1483552676006227968,'Dengue'),(1483552676027199488,'Diphtérie'),(1483552676446629888,'Diphyllobothriose'),(1483552676463407104,'Dracunculose'),(1483552676962529280,'Maladie à virus Ebola'),(1483552676983500800,'Echinococcose'),(1483552677360988160,'Ehrlichiose'),(1483552677461651456,'Encéphalite de Saint-Louis'),(1483552677818167296,'Encéphalite à tiques ou méningoencéphalite à tiques'),(1483552677818167297,'Encéphalite japonaise'),(1483552678346649600,'Enterobiase'),(1483552678371815424,'Enterococcus'),(1483552678929657856,'Entérovirus'),(1483552678938046464,'Encéphalopathie spongiforme bovine'),(1483552679328116736,'Escherichia coli'),(1483552679370059776,'Fasciolopsiose'),(1483552679923707904,'Fasciolose'),(1483552679944679424,'Insomnie fatale familiale'),(1483552680301195264,'Fièvre de Kyasanur'),(1483552680322166784,'Fièvre de Crimée-Congo'),(1483552680741597184,'Fièvre hémorragique d\'Omsk'),(1483552680741597185,'Filariose'),(1483552682478039040,'Intoxication alimentaire par Clostridium perfringens'),(1483552682486427648,'Fusobacterium'),(1483552684164149248,'Vaginose'),(1483552685871230976,'Syndrome de Gerstmann-Sträussler-Scheinker'),(1483552685871230977,'Gangrène gazeuse'),(1483552686248718336,'Morve'),(1483552686349381632,'Gonorrhée'),(1483552687947411456,'Méningocoque'),(1483552688018714624,'Streptocoque A'),(1483552688400396288,'Streptocoque B'),(1483552688475893760,'Streptococcus pneumoniae'),(1483552688807243776,'Haemophilus influenzae'),(1483552688912101376,'Haemophilus parainfluenzae'),(1483552690602405888,'Syndrome pieds-mains-bouche'),(1483552690623377408,'Hantavirus'),(1483552691063779328,'Helicobacter pylori'),(1483552691139276800,'Syndrome hémolytique et urémique'),(1483552692749889536,'Henipavirus'),(1483552692800221184,'Virus Hendra'),(1483552694184341504,'Hépatite A'),(1483552694259838976,'Hépatite B'),(1483552694712823808,'Hépatite C'),(1483552694717018112,'Hépatite D'),(1483552695094505472,'Hépatite E'),(1483552695140642816,'Hépatite G'),(1483552695497158656,'Herpes simplex'),(1483552695513935872,'Histoplasmose'),(1483552695853674496,'Ankylostomose'),(1483552695870451712,'Bocavirus humain'),(1483552696189218816,'Métapneumovirus'),(1483552696327630848,'Ehrlichiose monocytique humaine'),(1483552696709312512,'Parainfluenza'),(1483552696772227072,'Papillomavirus humain'),(1483552698559000576,'Hymenolepis'),(1483552698579972096,'Mononucléose infectieuse du virus d\'Epstein-Barr'),(1483552699003596800,'Grippe'),(1483552699016179712,'Isosporose'),(1483552699334946816,'Maladie de Kawasaki'),(1483552699372695552,'Kératite'),(1483552699846651904,'Kingella kingae'),(1483552699850846208,'Kuru'),(1483552700224139264,'Fièvre de Lassa'),(1483552700266082304,'Klebsiellose'),(1483552700622598144,'Légionellose'),(1483552700706484224,'Legionellose'),(1483552701151080448,'Leishmaniose'),(1483552701163663360,'Lèpre'),(1483552701599870976,'Leptospirose'),(1483552701662785536,'Listériose'),(1483552702040272896,'Borréliose'),(1483552702094798848,'Filariose'),(1483552702413565952,'Chorioméningite lymphocytaire'),(1483552702493257728,'Malaria'),(1483552702870745088,'Marburg'),(1483552702891716608,'Rougeole'),(1483552703298564096,'Mélioïdose'),(1483552703311147008,'Méningite'),(1483552704997257216,'Douve de Yokogawa'),(1483552705219555328,'Microsporidiose'),(1483552707534811136,'Molluscum contagiosum'),(1483552707690000384,'Lambliase'),(1483552708935708672,'Pneumonie mycoplasme'),(1483552709212532736,'Myiase'),(1483552709237698560,'Conjonctivite néonatale'),(1483552709392887808,'Oreillons'),(1483552710944780288,'Maladie de Creutzfeldt-Jakob'),(1483552711158689792,'Géotrichose'),(1483552711519399936,'Parvovirus B19'),(1483552712135962624,'Pasteurellose'),(1483552713448779776,'Onchocercose'),(1483552713452974080,'Pediculosis capitis'),(1483552714803539968,'Paracoccidioidomycose'),(1483552714920980480,'Mycétome'),(1483552716414152704,'Pediculosis corporis'),(1483552717747941376,'Pou du pubis'),(1483552719194976256,'Coqueluche'),(1483552719241113600,'Pneumocystose'),(1483552720713314304,'Pneumonie'),(1483552720830754816,'Poliomyélite'),(1483552721984188416,'Prevotella'),(1483552722307149824,'Méningo-encéphalite amibienne primitive'),(1483552722554613760,'Leucoencéphalopathie multifocale progressive'),(1483552722797883392,'Ornithose'),(1483552722974044160,'Fièvre Q'),(1483552723036958720,'Rage'),(1483552723330560000,'Rhinosporidiose'),(1483552723334754304,'Fièvre par morsure de rat'),(1483552723749990400,'Rhinovirus'),(1483552723749990401,'Rickettsial'),(1483552725419323392,'Fièvre pourprée des montagnes Rocheuses'),(1483552725419323393,'Fièvre de la vallée du Rift'),(1483552725821976576,'Rotavirus'),(1483552725855531008,'Rubéole'),(1483552726253989888,'Salmonellose'),(1483552726291738624,'Syndrome respiratoire aigu sévère'),(1483552726774083584,'Maladie à coronavirus 2019'),(1483552726807638016,'Gale'),(1483552727130599424,'Bilharziose'),(1483552727185125376,'Septicémie'),(1483552727466143744,'Shigellose'),(1483552727545835520,'Zona'),(1483552727902351360,'Variole'),(1483552727923322880,'Scarlatine'),(1483552728263061504,'Sporotrichose'),(1483552728305004544,'Staphylococcus'),(1483552728619577344,'Syphilis'),(1483552728678297600,'Syndrome respiratoire du Moyen-Orient'),(1483552728980287488,'Taeniasis'),(1483552729076756480,'Tétanos'),(1483552729475215360,'Tinea barbae'),(1483552729500381184,'Tinea capitis'),(1483552729848508416,'Tinea cruris'),(1483552729852702720,'Tinea corporis'),(1483552730267938816,'Tinea manuum'),(1483552731601727488,'Tinea nigra'),(1483552731979214848,'Tinea pedis'),(1483552732058906624,'Tinea unguium'),(1483552732415422464,'Tinea versicolor'),(1483552732478337024,'Toxocariase'),(1483552732792909824,'Toxocariase'),(1483552732876795904,'Toxoplasmose'),(1483552733250088960,'Trichinellose'),(1483552733275254784,'Trichomonase'),(1483552733627576320,'Trichuriase'),(1483552733648547840,'Tuberculose'),(1483552734009257984,'Tularémie'),(1483552734072172544,'Ureaplasma urealyticum'),(1483552734407716864,'Encéphalite équine vénézuelienne'),(1483552734445465600,'Fièvre hémorragique vénézuélienne'),(1483552734843924480,'Pneumonie virale'),(1483552734860701696,'Fièvre du Nil occidental'),(1483552735162691584,'Infection à virus Nipah'),(1483552735259160576,'Virus respiratoire syncytial'),(1483552735716339712,'Yersinia pseudotuberculosis'),(1483552735716339713,'Piedra blanche'),(1483552736152547328,'Yersiniose'),(1483552736177713152,'Fièvre jaune'),(1483552736555200512,'Infection à virus Zika'),(1483552736571977728,'Zygomycose'),(1483552739306663936,'Pseudomonas Aeruginosa'),(1483552739491213312,'Roséole'),(1483552739491213313,'Peste');
/*!40000 ALTER TABLE `SD_DISEASE` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER trig_sd_disease_update
    BEFORE UPDATE
    ON SD_DISEASE
    FOR EACH ROW
BEGIN
   IF NEW.DIS_LABEL = '' THEN
       SET NEW.DIS_LABEL = OLD.DIS_LABEL;
end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `SD_DOCTOR`
--

DROP TABLE IF EXISTS `SD_DOCTOR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SD_DOCTOR` (
  `DOC_ID` bigint(4) NOT NULL,
  `DOC_LASTNAME` varchar(64) NOT NULL,
  `DOC_FIRSTNAME` varchar(64) NOT NULL,
  PRIMARY KEY (`DOC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SD_DOCTOR`
--

LOCK TABLES `SD_DOCTOR` WRITE;
/*!40000 ALTER TABLE `SD_DOCTOR` DISABLE KEYS */;
INSERT INTO `SD_DOCTOR` VALUES (1481984036686532608,'ATAN','Charles'),(1483546575210340352,'LEDOCTEUR','bob'),(1483552059929714688,'TEST','Test');
/*!40000 ALTER TABLE `SD_DOCTOR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SD_MEDICINE`
--

DROP TABLE IF EXISTS `SD_MEDICINE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SD_MEDICINE` (
  `MED_NUM` bigint(4) NOT NULL,
  `MED_LABEL` varchar(128) NOT NULL,
  `MED_LABORATORY` varchar(64) DEFAULT NULL,
  `MED_COMMENT` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`MED_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SD_MEDICINE`
--

LOCK TABLES `SD_MEDICINE` WRITE;
/*!40000 ALTER TABLE `SD_MEDICINE` DISABLE KEYS */;
INSERT INTO `SD_MEDICINE` VALUES (1483557183646248960,'EFFERALGAN','idk','granulés en sachet est indiqué dans le traitement symptomatique des douleurs d\'intensité légère à modérée et/ou des états fébriles. '),(1483557183667220480,'DOLIPRANE','idk','Traitement symptomatique des douleurs d\'intensité légère à modérée et/ou des états fébriles. '),(1483557187031052288,'SPEDIFEN','idk','Ce médicament contient un anti-inflammatoire non stéroïdien : l\'ibuprofène. '),(1483557187031052289,'ISIMIG','idk','Traitement de la phase céphalalgique de la crise de migraine avec ou sans aura. '),(1483557187198824448,'DAFALGAN','idk','Traitement symptomatique des douleurs d\'intensité légère à modérée et/ou des états fébriles. '),(1483557188587139072,'ELUDRIL','idk','Traitement local d\'appoint antibactérien et antalgique des affections limitées à la muqueuse buccale et à l\'oropharynx'),(1483557188599721984,'IXPRIM','idk','IXPRIM est indiqué dans le traitement symptomatique des douleurs modérées à intenses. '),(1483557190428438528,'LEVOTHYROX','idk','Circonstances, associées ou non à une hypothyroïdie, où il est nécessaire de freiner la TSH. '),(1483557190449410048,'PARACETAMOL BIOGARAN','idk','Traitement symptomatique des douleurs d\'intensité légère à modérée et/ou des états fébriles. '),(1483557190868840448,'MAGNE B6','idk','Carences magnésiennes avérées, isolées ou associées. '),(1483557190906589184,'PIASCLEDINE','idk','traitement symptomatique à effet différé de l\'arthrose de la hanche et du genou. '),(1483557192458481664,'KARDEGIC','idk','Syndromes coronariens aigus et à la phase aiguë de l\'infarctus du myocarde, notamment lorsque la voie orale ne peut être utilisée. '),(1483557192575922176,'IMODIUM','idk','Traitement symptomatique des diarrhées aiguës et chroniques. '),(1483557192814997504,'LAMALINE','idk','Traitement symptomatique des douleurs d\'intensité modérée à intense et/ou ne répondant pas à l\'utilisation d\'antalgiques périphériques utilisés seuls.'),(1483557192856940544,'GAVISCON','idk','Traitement symptomatique du reflux gastro-oesophagien.'),(1483557194773737472,'ANTARENE','idk','Traitement symptomatique en traumatologie bénigne: entorses, contusions. '),(1483557195302219776,'RHINOFLUIMUCIL','idk','Traitement local symptomatique de courte durée des affections rhinopharyngées avec sécrétion excessive de la muqueuse de l\'adulte et des adolescents de plus de 15 ans. '),(1483557195876839424,'SUBUTEX','idk','Traitement substitutif de la pharmacodépendance aux opioïdes, dans le cadre d\'une thérapeutique globale de prise en charge médicale, sociale et psychologique. '),(1483557196153663488,'DAFLON','idk','Traitement symptomatique en traumatologie bénigne: entorses, contusions.'),(1483557196644397056,'METEOSPASMYL','idk','Traitement symptomatique des manifestations fonctionnelles intestinales notamment avec météorisme. '),(1483557196652785664,'TOPLEXIL','idk','Traitement symptomatique des toux non productives gênantes en particulier à prédominance nocturne. '),(1483557197051244544,'PIVALONE','idk','Manifestations inflammatoires et allergiques du rhino-pharynx: rhinites allergiques, rhinites saisonnières, rhinites congestives aiguës et chroniques, rhinites vaso-motrices.'),(1483557197076410368,'ADVIL','idk','Il est indiqué, chez l\'adulte et l\'enfant de plus de 20 kg, dans le traitement de courte durée de la fièvre'),(1483557197609086976,'XANAX','idk','Traitement symptomatique des manifestations anxieuses sévères et/ou invalidantes'),(1483557211647422464,'FORLAX','idk','Traitement symptomatique de la constipation chez l\'adulte et chez l\'enfant à partir de 8 ans. ');
/*!40000 ALTER TABLE `SD_MEDICINE` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER trig_sd_medicine_update
    BEFORE UPDATE
    ON SD_MEDICINE
    FOR EACH ROW
BEGIN
    IF NEW.MED_COMMENT = '' THEN
        SET NEW.MED_COMMENT = OLD.MED_COMMENT;
    END IF;

    IF NEW.MED_LABEL = '' THEN
        SET NEW.MED_LABEL = OLD.MED_LABEL;
    END IF;

    IF NEW.MED_LABORATORY = '' THEN
        SET NEW.MED_LABORATORY = OLD.MED_LABORATORY;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `SD_PATIENT`
--

DROP TABLE IF EXISTS `SD_PATIENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SD_PATIENT` (
  `PAT_ID` bigint(4) NOT NULL,
  `DOC_ID` bigint(4) NOT NULL,
  `PAT_LASTNAME` varchar(32) NOT NULL,
  `PAT_FIRSTNAME` varchar(32) NOT NULL,
  `PAT_BIRTHDATE` date NOT NULL,
  `PAT_GENDER` char(1) NOT NULL CHECK (`PAT_GENDER` in ('H','F')),
  `PAT_ADDRESS_LINES` varchar(256) DEFAULT NULL,
  `PAT_LOCALITY` varchar(64) DEFAULT NULL,
  `PAT_REGION` varchar(64) DEFAULT NULL,
  `PAT_ZIPCODE` int(5) DEFAULT NULL,
  `PAT_COUNTRY` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`PAT_ID`),
  KEY `FK_SD_PATIENT_SD_DOCTOR` (`DOC_ID`),
  CONSTRAINT `FK_SD_PATIENT_SD_DOCTOR` FOREIGN KEY (`DOC_ID`) REFERENCES `SD_DOCTOR` (`DOC_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SD_PATIENT`
--

LOCK TABLES `SD_PATIENT` WRITE;
/*!40000 ALTER TABLE `SD_PATIENT` DISABLE KEYS */;
INSERT INTO `SD_PATIENT` VALUES (1483560900134674432,1483552059929714688,'Mattecot','Anaé','2013-07-06','H','6269 Sutteridge Pass','Guérande','Pays de la Loire',44356,'France'),(1483560900134674433,1483552059929714688,'Hair','Edmée','2016-06-18','F','23 Kedzie Trail','Sarrebourg','Lorraine',57404,'France'),(1483560902022111232,1483552059929714688,'Oty','Marie-ève','1969-06-29','H','5 Reinke Lane','Villeneuve-d\'Ascq','Nord-Pas-de-Calais',59663,'France'),(1483560902038888448,1483552059929714688,'Rubertelli','Mélia','1997-05-15','H','17 Briar Crest Drive','Mérignac','Aquitaine',33709,'France'),(1483560903032938496,1483552059929714688,'Potkins','Agnès','1991-05-27','F','57 Everett Alley','Paris 14','Île-de-France',75669,'France'),(1483560903032938497,1483552059929714688,'Dugall','Néhémie','2000-07-04','H','75 Comanche Plaza','Bonneuil-sur-Marne','Île-de-France',94384,'France'),(1483560904752603136,1483552059929714688,'Ford','Faîtes','1968-12-17','F','0056 Surrey Street','La Courneuve','Île-de-France',93126,'France'),(1483560904752603137,1483552059929714688,'Le Huquet','Bérénice','2010-01-23','F','91 Sommers Center','Vincennes','Île-de-France',94309,'France'),(1483560904752603138,1483552059929714688,'Abbiss','Loïs','1995-10-30','H','6 Lakewood Pass','Bourg-en-Bresse','Rhône-Alpes',1004,'France'),(1483560904895209472,1483552059929714688,'Mapowder','Simplifiés','1955-08-04','F','6 Huxley Place','Romorantin-Lanthenay','Centre',41204,'France');
/*!40000 ALTER TABLE `SD_PATIENT` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER trig_sd_patient_update
    BEFORE UPDATE
    ON SD_PATIENT
    FOR EACH ROW
BEGIN
    IF NEW.PAT_LOCALITY = '' THEN
        SET NEW.PAT_LOCALITY = OLD.PAT_LOCALITY;
    END IF;

    IF NEW.PAT_ADDRESS_LINES = '' THEN
        SET NEW.PAT_ADDRESS_LINES = OLD.PAT_ADDRESS_LINES;
    END IF;

    IF NEW.PAT_REGION = '' THEN
        SET NEW.PAT_REGION = OLD.PAT_REGION;
    END IF;

    IF NEW.PAT_ZIPCODE = '' THEN
        SET NEW.PAT_ZIPCODE = OLD.PAT_ZIPCODE;
    END IF;

    IF NEW.PAT_COUNTRY = '' THEN
        SET NEW.PAT_COUNTRY = OLD.PAT_COUNTRY;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `SD_PRESCRIPTION`
--

DROP TABLE IF EXISTS `SD_PRESCRIPTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SD_PRESCRIPTION` (
  `PRE_NUM` bigint(4) NOT NULL,
  `APT_NUM` bigint(4) NOT NULL,
  `MED_NUM` bigint(4) NOT NULL,
  `PRE_DAILY_QTY` int(2) NOT NULL,
  `PRE_DURATION` int(2) NOT NULL,
  PRIMARY KEY (`PRE_NUM`),
  KEY `FK_SD_PRESCRIPTION_SD_APPOINTMENT` (`APT_NUM`),
  KEY `FK_SD_PRESCRIPTION_SD_MEDICINE` (`MED_NUM`),
  CONSTRAINT `FK_SD_PRESCRIPTION_SD_APPOINTMENT` FOREIGN KEY (`APT_NUM`) REFERENCES `SD_APPOINTMENT` (`APT_NUM`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SD_PRESCRIPTION_SD_MEDICINE` FOREIGN KEY (`MED_NUM`) REFERENCES `SD_MEDICINE` (`MED_NUM`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SD_PRESCRIPTION`
--

LOCK TABLES `SD_PRESCRIPTION` WRITE;
/*!40000 ALTER TABLE `SD_PRESCRIPTION` DISABLE KEYS */;
/*!40000 ALTER TABLE `SD_PRESCRIPTION` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/  /*!50003 TRIGGER trig_sd_prescription
    BEFORE UPDATE
    ON SD_PRESCRIPTION
    FOR EACH ROW
BEGIN
    IF NEW.MED_NUM = 0 OR NEW.MED_NUM = '' THEN
        SET NEW.MED_NUM = OLD.MED_NUM;
    end if;

    IF NEW.PRE_DAILY_QTY = '0' OR NEW.PRE_DAILY_QTY = '' THEN
        SET NEW.PRE_DAILY_QTY = OLD.PRE_DAILY_QTY;
    end if;

    IF NEW.PRE_DURATION = '0' OR NEW.PRE_DURATION = '' THEN
        SET NEW.PRE_DURATION = OLD.PRE_DURATION;
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `SD_V_NUM_PATIENT_PER_DISEASE`
--

DROP TABLE IF EXISTS `SD_V_NUM_PATIENT_PER_DISEASE`;
/*!50001 DROP VIEW IF EXISTS `SD_V_NUM_PATIENT_PER_DISEASE`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `SD_V_NUM_PATIENT_PER_DISEASE` (
  `DIS_NUM` tinyint NOT NULL,
  `DIS_LABEL` tinyint NOT NULL,
  `NUMBER` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `SD_V_PATIENT_EX`
--

DROP TABLE IF EXISTS `SD_V_PATIENT_EX`;
/*!50001 DROP VIEW IF EXISTS `SD_V_PATIENT_EX`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `SD_V_PATIENT_EX` (
  `PAT_ID` tinyint NOT NULL,
  `DOC_ID` tinyint NOT NULL,
  `PAT_LASTNAME` tinyint NOT NULL,
  `PAT_FIRSTNAME` tinyint NOT NULL,
  `PAT_BIRTHDATE` tinyint NOT NULL,
  `PAT_GENDER` tinyint NOT NULL,
  `PAT_ADDRESS_LINES` tinyint NOT NULL,
  `PAT_LOCALITY` tinyint NOT NULL,
  `PAT_REGION` tinyint NOT NULL,
  `PAT_ZIPCODE` tinyint NOT NULL,
  `PAT_COUNTRY` tinyint NOT NULL,
  `AGE` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `SD_V_PRESCRIPTION`
--

DROP TABLE IF EXISTS `SD_V_PRESCRIPTION`;
/*!50001 DROP VIEW IF EXISTS `SD_V_PRESCRIPTION`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `SD_V_PRESCRIPTION` (
  `MED_NUM` tinyint NOT NULL,
  `PRE_NUM` tinyint NOT NULL,
  `APT_NUM` tinyint NOT NULL,
  `PRE_DAILY_QTY` tinyint NOT NULL,
  `PRE_DURATION` tinyint NOT NULL,
  `MED_LABEL` tinyint NOT NULL,
  `MED_LABORATORY` tinyint NOT NULL,
  `MED_COMMENT` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `SD_V_NUM_PATIENT_PER_DISEASE`
--

/*!50001 DROP TABLE IF EXISTS `SD_V_NUM_PATIENT_PER_DISEASE`*/;
/*!50001 DROP VIEW IF EXISTS `SD_V_NUM_PATIENT_PER_DISEASE`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`esteban`@`77.198.90.62` SQL SECURITY DEFINER */
/*!50001 VIEW `SD_V_NUM_PATIENT_PER_DISEASE` AS select `D`.`DIS_NUM` AS `DIS_NUM`,`D`.`DIS_LABEL` AS `DIS_LABEL`,count(`SP`.`PAT_ID`) AS `NUMBER` from (((`SD_DISEASE` `D` left join `SD_DIAGNOSE` `SD` on(`D`.`DIS_NUM` = `SD`.`DIS_NUM`)) left join `SD_APPOINTMENT` `SA` on(`SD`.`APT_NUM` = `SA`.`APT_NUM`)) left join `SD_PATIENT` `SP` on(`SA`.`PAT_ID` = `SP`.`PAT_ID`)) group by `D`.`DIS_NUM`,`D`.`DIS_LABEL` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `SD_V_PATIENT_EX`
--

/*!50001 DROP TABLE IF EXISTS `SD_V_PATIENT_EX`*/;
/*!50001 DROP VIEW IF EXISTS `SD_V_PATIENT_EX`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`esteban`@`77.198.90.62` SQL SECURITY DEFINER */
/*!50001 VIEW `SD_V_PATIENT_EX` AS select `SD_PATIENT`.`PAT_ID` AS `PAT_ID`,`SD_PATIENT`.`DOC_ID` AS `DOC_ID`,`SD_PATIENT`.`PAT_LASTNAME` AS `PAT_LASTNAME`,`SD_PATIENT`.`PAT_FIRSTNAME` AS `PAT_FIRSTNAME`,`SD_PATIENT`.`PAT_BIRTHDATE` AS `PAT_BIRTHDATE`,`SD_PATIENT`.`PAT_GENDER` AS `PAT_GENDER`,`SD_PATIENT`.`PAT_ADDRESS_LINES` AS `PAT_ADDRESS_LINES`,`SD_PATIENT`.`PAT_LOCALITY` AS `PAT_LOCALITY`,`SD_PATIENT`.`PAT_REGION` AS `PAT_REGION`,`SD_PATIENT`.`PAT_ZIPCODE` AS `PAT_ZIPCODE`,`SD_PATIENT`.`PAT_COUNTRY` AS `PAT_COUNTRY`,year(sysdate()) - year(`SD_PATIENT`.`PAT_BIRTHDATE`) - (dayofyear(sysdate()) < dayofyear(`SD_PATIENT`.`PAT_BIRTHDATE`)) AS `AGE` from `SD_PATIENT` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `SD_V_PRESCRIPTION`
--

/*!50001 DROP TABLE IF EXISTS `SD_V_PRESCRIPTION`*/;
/*!50001 DROP VIEW IF EXISTS `SD_V_PRESCRIPTION`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`esteban`@`77.198.90.62` SQL SECURITY DEFINER */
/*!50001 VIEW `SD_V_PRESCRIPTION` AS select `SD_PRESCRIPTION`.`MED_NUM` AS `MED_NUM`,`SD_PRESCRIPTION`.`PRE_NUM` AS `PRE_NUM`,`SD_PRESCRIPTION`.`APT_NUM` AS `APT_NUM`,`SD_PRESCRIPTION`.`PRE_DAILY_QTY` AS `PRE_DAILY_QTY`,`SD_PRESCRIPTION`.`PRE_DURATION` AS `PRE_DURATION`,`SM`.`MED_LABEL` AS `MED_LABEL`,`SM`.`MED_LABORATORY` AS `MED_LABORATORY`,`SM`.`MED_COMMENT` AS `MED_COMMENT` from (`SD_PRESCRIPTION` join `SD_MEDICINE` `SM` on(`SD_PRESCRIPTION`.`MED_NUM` = `SM`.`MED_NUM`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-18 23:21:42
