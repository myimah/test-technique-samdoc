-- MySQL dump 10.19  Distrib 10.3.32-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sd_auth
-- ------------------------------------------------------
-- Server version	10.5.13-MariaDB-0ubuntu0.21.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AUTH_TOKEN`
--

DROP TABLE IF EXISTS `AUTH_TOKEN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUTH_TOKEN` (
  `TOK_TOKEN` varchar(128) NOT NULL,
  `USR_ID` bigint(4) NOT NULL,
  `TOK_EXPIRATION` datetime NOT NULL DEFAULT (sysdate() + interval 1 day),
  PRIMARY KEY (`TOK_TOKEN`),
  KEY `FK_AUTH_TOKEN_AUTH_USER` (`USR_ID`),
  CONSTRAINT `FK_AUTH_TOKEN_AUTH_USER` FOREIGN KEY (`USR_ID`) REFERENCES `AUTH_USER` (`USR_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTH_TOKEN`
--

LOCK TABLES `AUTH_TOKEN` WRITE;
/*!40000 ALTER TABLE `AUTH_TOKEN` DISABLE KEYS */;
/*!40000 ALTER TABLE `AUTH_TOKEN` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50003 TRIGGER trig_auth_token_insert BEFORE INSERT ON AUTH_TOKEN FOR EACH ROW
    BEGIN
        DECLARE num_tokens INT;

        SELECT COUNT(*) INTO num_tokens FROM AUTH_TOKEN WHERE TOK_EXPIRATION > SYSDATE() AND USR_ID = NEW.USR_ID;

        IF num_tokens <> 0 THEN
            SIGNAL SQLSTATE '45001' SET MESSAGE_TEXT = 'Cannot create a token while previous one hasn''t been revoked.';
        END IF;

#         DELETE FROM AUTH_TOKEN WHERE TOK_EXPIRATION < SYSDATE();
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `AUTH_USER`
--

DROP TABLE IF EXISTS `AUTH_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUTH_USER` (
  `USR_ID` bigint(4) NOT NULL,
  `USR_USERNAME` varchar(32) NOT NULL,
  `USR_PASSWORD` varchar(64) NOT NULL,
  `USR_SALT` varchar(10) NOT NULL,
  PRIMARY KEY (`USR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUTH_USER`
--

LOCK TABLES `AUTH_USER` WRITE;
/*!40000 ALTER TABLE `AUTH_USER` DISABLE KEYS */;
INSERT INTO `AUTH_USER` VALUES (1483552059929714688,'test','7acb68b43940639603c96a1bd246617be2e739cc74d4696ffdbe3ba43c87006a','mtGrTDXngO');
/*!40000 ALTER TABLE `AUTH_USER` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-18 23:23:14
